import torch
from dataset import data_loader as loader
# from dataset import generateMeteor, generatenbody
from torch_geometric.data import Data
from torch_geometric.datasets import TUDataset
from torch_geometric.datasets import Planetoid
import torch.nn.functional as F
from torch.nn.functional import mse_loss as MSELoss
from torch.nn.functional import l1_loss as MAELoss
from methods.network import Net, GCN_Predict, TACA, Encoder
from utils.util import get_init_idx, after_random_quired_idx, \
    after_kcenter_quired_idx, get_unlabeled_logits, get_init_idx_k_center_greedy, \
    aggregation_function, train_test_split
import torch_geometric.transforms as T
from torch_geometric.nn import GCNConv, GAE, VGAE
from utils.train_test_split_edges import train_test_split_edges
import numpy as np
from methods.Queryer import K_Center_Greedy
import pickle
import time

torch.manual_seed(0)

N_Query =100
K = 10
maxEpoch = 100
N_Init = 50
HOLD_OUT_RATIO = 0.1
DATASET = 'RayTrace'
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


if DATASET == 'RayTrace':
    X, y = loader.meteor()
    from dataset import generateMeteor as generator
elif DATASET == 'Nbody':
    X, y = loader.nbody()
    from dataset import generatenbody as generator
elif DATASET == 'RayTrace':
    X, y = loader.raytrace()
    from dataset import  generateRayTrace as generator


graph = generator.callgraph
train_pos_edge_index = torch.tensor(np.array(graph), dtype=torch.long).to(device)
num_node, num_feature = int(np.amax(graph)+1), X.shape[1]
test_mask = train_test_split(X.shape[0], ratio = HOLD_OUT_RATIO)



Net_Architecture = [num_feature,
                    8, 4, 1] # number of neurons in output layer is 1 for regression
predictor = GCN_Predict(Net_Architecture).to(device)
loss_fn = torch.nn.MSELoss()
optimizer_for_pred = torch.optim.Adam(predictor.parameters(), lr=0.01, weight_decay=5e-4)


start_time = time.time()
with open(str(DATASET)+'_embedded.result', 'rb') as f:
    embedded_X = pickle.load(f)


data = Data(x = embedded_X.float(),
            edge_index = train_pos_edge_index,
            y = torch.from_numpy(y).float().to(device) )
data.test_mask = test_mask
data.train_mask = get_init_idx_k_center_greedy(data, data.test_mask, K, N_Init).to(device)


predictor.train()
for query in range(N_Query):
    i_center = query % K
    for epoch in range(maxEpoch):
        optimizer_for_pred.zero_grad()
        logit, out = predictor(data)
        mse_loss = loss_fn(out[data.train_mask], data.y[data.train_mask])
        mse_loss.backward()
        optimizer_for_pred.step()
        if epoch == 1:
            # Here we query the data points in the k-center greedy way
            unlabel_logits = get_unlabeled_logits(logit, data.train_mask, data.test_mask)
            # print(unlabel_logits)
            near_centroids_idx = K_Center_Greedy(unlabel_logits, K)
            # print('done')
            data.train_mask = after_kcenter_quired_idx(data.train_mask, near_centroids_idx, i_center).to(device)

    predictor.eval()
    with torch.no_grad():
        _, pred = predictor(data)
        error = MAELoss(pred[data.test_mask], data.y[data.test_mask]).item()
        print('Prediction Error (in sec): {:.4f}'.format(error))

    print('Epoch # {:.1f}, loss: {:.4f}'.format(epoch, mse_loss.item()))
    labeled_points = data.train_mask.sum().item()
    print('Queried {:.1f} # of instances'.format(labeled_points))

finish_time = time.time()

print()
print("Overall, it takes {:.2f} seconds".format(finish_time - start_time))




