import numpy as np
import torch
import math
from methods.Queryer import K_Center_Greedy
np.random.seed(0)

n_per_quary = 1

def get_init_idx(test_mask_tensor, num_init):
    test_idx_arr = test_mask_tensor.data.cpu().numpy()
    candidates = np.where(test_idx_arr == False)
    random_list = np.random.choice(candidates[0], num_init, replace=False)
    init_idx = np.zeros(test_idx_arr.shape[0], dtype=bool)
    init_idx[random_list] = True
    return torch.from_numpy(init_idx)

def get_init_idx_k_center_greedy(data, test_mask_tensor, k_clusters, num_init):
    test_idx_arr = test_mask_tensor.data.cpu().numpy()
    init_idx = np.zeros(test_idx_arr.shape[0], dtype=bool)
    n_fold, scatters = int(num_init / k_clusters), num_init % k_clusters
    for i in range(n_fold):
        candidates = np.where(test_idx_arr == False)  # indexes
        data_tensor = data.x[candidates]
        near_centroids_idx = K_Center_Greedy(data_tensor, k_clusters)
        init_idx[near_centroids_idx] = True
        test_idx_arr[near_centroids_idx] = True
        if i == n_fold-1:
            candidates = np.where(test_idx_arr == False)  # indexes
            data_tensor = data.x[candidates]
            near_centroids_idx = K_Center_Greedy(data_tensor, k_clusters)
            for j in range(scatters):
                init_idx[near_centroids_idx[j]] = True
    return torch.from_numpy(init_idx)




def after_random_quired_idx(mask_tensor):
    mask_arr = mask_tensor.data.cpu().numpy()
    candidates = np.where(mask_arr == False)
    random_list = np.random.choice(candidates[0], n_per_quary, replace=False)
    mask_arr[random_list] = True
    return torch.from_numpy(mask_arr)

def get_unlabeled_logits(logits, mask_tensor, test_mask):
    mask_arr = mask_tensor.data.cpu().numpy()
    test_arr = test_mask.data.cpu().numpy()
    candidates = np.where((mask_arr == False) & (test_arr == False))
    # print(logits[candidates])
    return logits[candidates]


def after_kcenter_quired_idx(mask_tensor, queries_list, i_cluster):
    mask_arr = mask_tensor.data.cpu().numpy()
    candidates = np.where(mask_arr == False)
    mask_arr[queries_list[i_cluster]] = True
    return torch.from_numpy(mask_arr)

def get_train_idx(num_nodes):
    train_index = [0, 1, 2]
    train_idx = np.zeros(num_nodes, dtype=bool)
    train_idx[train_index] = True
    return torch.from_numpy(train_idx)

def get_pool_idx(num_nodes):
    pool_size = np.arange(num_nodes)
    pool_idx = np.zeros(num_nodes, dtype=bool)
    pool_idx[pool_size] = False
    return pool_idx


def aggregation_function(tensor_2d, method = 'average'):
    if method == 'average':
        return torch.mean(tensor_2d, dim=0)
    elif method == 'concat':
        return tensor_2d.view(-1)

def train_test_split(num_sample, ratio = .1):
    pool_idx = np.zeros(num_sample, dtype=bool)
    num_test = int(num_sample * ratio)
    candidates = np.where(pool_idx == False)
    test_list = np.random.choice(candidates[0], num_test, replace=False)
    pool_idx[test_list] = True
    return torch.from_numpy(pool_idx) # test.mask




if __name__ == "__main__":

    b = get_init_idx(100)
    a = np.where(b == True)