from dataset import data_loader as loader
import matplotlib.pyplot as plt
from matplotlib import rcParams
import numpy as np
from sklearn.manifold import TSNE


rcParams['font.family'] = 'Times New Roman'
rcParams['font.size'] = 25
markerSize = 50
rcParams["legend.columnspacing"] = 0.1
# plt.rc('xtick', labelsize=24)
# plt.rc('ytick', labelsize=24)
# plt.rcParams['text.usetex'] = True
colors = ['#3282b8', '#fe9801']
DATASET = 'RayTrace'

if DATASET == 'Meteor':
    X, y = loader.meteor()
elif DATASET == 'RayTrace':
    X, y = loader.raytrace(clf=True)
elif DATASET == "NBody":
    X, y = loader.nbody()



fig = plt.figure(figsize=(12, 8))
ax = fig.add_subplot(1, 1, 1)

def plot_feature_nonlinear():
    for i in range(len(X[0])):
    # for i in [3]:
        config = X[:, i]
        runtime = y.T

        ax.scatter(config, runtime)
        ax.set_xticks([-.5, 0, 1, 1.5])
        ax.set_xlabel("Feature #" + str(i))
        ax.set_ylabel("Runtime (in seconds)")

        plt.show(block=False)
        plt.pause(.5)
        plt.close()
        fig.savefig(DATASET + "_feat_"+ str(i) + ".pdf", bbox_inches='tight')

def plot_label_hist():
    ax.hist(y, 50, alpha=0.75)
    p1, p2 = np.percentile(y, 25), np.percentile(y, 75)
    # y[y < p1], y[(y >= p1) & (y <= p2)], y[y > p2] = 0, 1, 2
    plt.grid(True)
    ax.set_xlabel('Runtime (in seconds)')
    ax.set_ylabel('Num. of instances')

    plt.axvline(x=p1, color='r', label='25% percentile')
    plt.axvline(x=p2, color='purple', label='75% percentile')
    fig.legend(loc='upper center', fancybox=False, markerscale=2.5, ncol=2,
               bbox_to_anchor=(0.48, 1, 0.1, 0.07), frameon=False,
               handletextpad=0.1)

    plt.show()
    fig.savefig(DATASET + "_labelCount.pdf", bbox_inches='tight')

def plot_diverse_pattern():
    global X, y
    X_G = TSNE(n_components=2).fit_transform(X)
    y_G = y.ravel()

    X, y = X_G[:50], y_G[:50]
    ax.scatter(X[y == 0, 0], X[y == 0, 1], label="Fast",  marker='*',s=markerSize, color='#d45d79')
    # ax.scatter(X[y == 1, 0], X[y == 1, 1], label="Medium", marker='*',s=markerSize)
    ax.scatter(X[y == 2, 0], X[y == 2, 1], label="Slow", marker='x',s=markerSize, color='#d45d79')

    X, y = X_G[51:100], y_G[51:100]
    ax.scatter(X[y == 0, 0], X[y == 0, 1], label="Fast", marker='^', s=markerSize, color='#5f6caf')
    # ax.scatter(X[y == 1, 0], X[y == 1, 1], label="Medium", marker='^', s=markerSize)
    ax.scatter(X[y == 2, 0], X[y == 2, 1], label="Slow", marker='v', s=markerSize, color='#5f6caf')

    X, y = X_G[101:600], y_G[101:600]
    ax.scatter(X[:,0], X[:,1], label="Unlabeled", marker='o', s=markerSize, alpha=0.3, color='#f0efef')

    plt.legend(loc=0)
    plt.show()
    fig.savefig(DATASET + "_TSNE.pdf", bbox_inches='tight')







if __name__ == "__main__":
    plot_diverse_pattern()

