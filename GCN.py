import torch
from dataset import data_loader as loader
from dataset import pytorchgemetric_dataset as Dataset
import torch.nn as nn
import torch.nn.functional as F
import time
from datetime import datetime
import torch_geometric.nn as pyg_nn
import torch_geometric.utils as pyg_utils
from torch_geometric.data import DataLoader
import torch_geometric.transforms as T

from tensorboardX import SummaryWriter
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
# from dataset import generateMeteor, generatenbody
from torch_geometric.data import Data
import random
import torch.optim as optim

# Load the bit vectors
X, y = loader.gregor()
print(len(X),len(X[0]),len(y))

# Load the call graph
from dataset import generate_gregor as generator 
graph = generator.callgraph

# Generate graph dataset using bit vectors and call graph
dataset=Dataset.generategraphdataset(X,y,graph)
tempdata=dataset[55]
number_of_node_features=len(tempdata.x[0])
print(number_of_node_features)

writer = SummaryWriter("./log/" + datetime.now().strftime("%Y%m%d-%H%M%S"))
random.shuffle(dataset)
task = 'graph'




# Graph neural network with 2 Graph convolution layer
class GNNStack(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim, task='node'):
        super(GNNStack, self).__init__()
        self.task = task
        self.convs = nn.ModuleList()
        self.convs.append(self.build_conv_model(input_dim, hidden_dim))
        self.lns = nn.ModuleList()
        self.lns.append(nn.LayerNorm(hidden_dim))
        self.lns.append(nn.LayerNorm(hidden_dim))
        for l in range(2):
            self.convs.append(self.build_conv_model(hidden_dim, hidden_dim))

        # post-message-passing
        self.post_mp = nn.Sequential(
            nn.Linear(hidden_dim, hidden_dim), nn.Dropout(0.25), 
            nn.Linear(hidden_dim, output_dim))
        if not (self.task == 'node' or self.task == 'graph'):
            raise RuntimeError('Unknown task.')

        self.dropout = 0.25
        self.num_layers = 3

    def build_conv_model(self, input_dim, hidden_dim):
        # refer to pytorch geometric nn module for different implementation of GNNs.
        if self.task == 'node':
            return pyg_nn.GCNConv(input_dim, hidden_dim)
        else:
            return pyg_nn.GINConv(nn.Sequential(nn.Linear(input_dim, hidden_dim),
                                  nn.ReLU(), nn.Linear(hidden_dim, hidden_dim)))

    def forward(self, data):
        x, edge_index, batch = data.x, data.edge_index, data.batch
        if data.num_node_features == 0:
          x = torch.ones(data.num_nodes, 1)

        for i in range(self.num_layers):
            x = self.convs[i](x, edge_index)
            emb = x
            x = F.relu(x)
            x = F.dropout(x, p=self.dropout, training=self.training)
            if not i == self.num_layers - 1:
                x = self.lns[i](x)

        if self.task == 'graph':
            x = pyg_nn.global_mean_pool(x, batch)
            

        x = self.post_mp(x)
        
        return emb, x.squeeze(1)

 # Not using it currently  
'''def test(loader, model, is_validation=False):
    model.eval()

    correct = 0
    for data in loader:
        with torch.no_grad():
            emb, pred = model(data)
            pred = pred.argmax(dim=1)
            label = data.y

        if model.task == 'node':
            mask = data.val_mask if is_validation else data.test_mask
            # node classification: only evaluate on nodes in test set
            pred = pred[mask]
            label = data.y[mask]
            
        correct += pred.eq(label).sum().item()
    
    if model.task == 'graph':
        total = len(loader.dataset) 
    else:
        total = 0
        for data in loader.dataset:
            total += torch.sum(data.test_mask).item()
    return correct / total'''

# start training of our network
def train(dataset, task, writer):
    if task == 'graph':
        data_size = len(dataset)
        loader = DataLoader(dataset[:int(data_size * 0.8)], batch_size=32, shuffle=True)
        test_loader = DataLoader(dataset[int(data_size * 0.8):], batch_size=32, shuffle=True)
    else:
        test_loader = loader = DataLoader(dataset, batch_size=64, shuffle=True)

    # build model
    model = GNNStack(max(number_of_node_features, 1), 32, 1, task=task)
    opt = optim.Adam(model.parameters(), lr=0.01)
    loss_func = torch.nn.L1Loss()
    # train
    train_pred=[]
    for epoch in range(200):
        total_loss = 0
        
        model.train()
        c=0
        for batch in loader:
            c=c+1
            #print(batch.train_mask, ' ----')
            opt.zero_grad()
            embedding, pred = model(batch)
            label = batch.y
            if task == 'node':
                pred = pred[batch.train_mask]
                label = label[batch.train_mask]
            loss = loss_func(pred, label)
            
            loss.backward()
            opt.step()
            total_loss += loss.item()
        total_loss /= len(loader.dataset)
        
        print("Total loss on epoch {} is {}".format(epoch,total_loss))
       
        writer.add_scalar("loss", total_loss, epoch)

        
    return model
model = train(dataset, task, writer)