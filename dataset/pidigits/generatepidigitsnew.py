import random
from functools import reduce
import subprocess
header = """
import itertools
import time
from six.moves import map as imap


DEFAULT_DIGITS = 2000
icount = itertools.count
islice = itertools.islice


def gen_x():
    #return imap(lambda k: (k, iadd(imult(4, k), 2), 0, iadd(imult(2, k), 1)), icount(1))
    return imap(lambda k: (k, 4 * k + 2, 0, 2 * k + 1), icount(1))

"""
callgraph = [[0, 1, 2, 2, 2, 2], [1, 2, 3, 4, 5, 6]]
def generate_header(function_name, param_names, param_anns, sub_vector):
    """
    String List[String] List[String] Slice[Bit] -> String
    This function accepts a function name, its corresponding parameter names,
    the possible annotations for these parameters, and a slice of the global
    parameter bit vector that corresponds to the parameter indexes.
    It produces a header with proper type annotations added
    """
    params = [(param_names[index] + param_anns[index] if value else param_names[index]) for index, value in enumerate(sub_vector)]
    param_string = ",".join(params)
    header = "def {0}({1})->Dyn:".format(function_name, param_string)
    return header

def generate_function(function_name, param_names, param_anns, function_body):
    """
    String List[String] List[String] -> (Slice[bit] -> String)
    This function generates a function body with annotations specified by sub_vector
    """
    return (lambda sub_vector: generate_header(function_name, param_names, param_anns, sub_vector) + function_body)
# Simple streams library.
# For building and using infinite lists.

compose_headers = ["def compose(a, b):",
                   "def compose(a:Tuple(int,int,int,int), b)->Tuple(int,int,int,int):",
                   "def compose(a, b:Tuple(int,int,int,int))->Tuple(int,int,int,int):",
                   "def compose(a:Tuple(int,int,int,int), b:Tuple(int,int,int,int))->Tuple(int,int,int,int):"]
compose_param = "a, b".split(",")
compose_anns = [":Tuple(int,int,int,int)",":Tuple(int,int,int,int)"]
compose_body = """    
    aq, ar, as_, at = a
    bq, br, bs, bt = b
    return (aq * bq + 0,
            aq * br + ar * bt + 0,
            as_ * bq + at * bs + 0,
            as_ * br + at * bt + 0)

"""
generate_compose= generate_function("compose", compose_param, compose_anns, compose_body)


extract_headers = ["def extract(z, j):",
                   "def extract(z:Tuple(int,int,int,int), j) -> int:",
                   "def extract(z, j:int) -> int:",
                   "def extract(z:Tuple(int,int,int,int), j:int) -> int:"]
extract_param = "z, j".split(",")
extract_param = [":Tuple(int,int,int,int)",":int"]
extract_body = """
    q, r, s, t = z
    #return idiv((iadd(imult(q, j), r)), (iadd(imult(s, j),  t)))
    return (q * j + r + 0) // (s * j + t + 0)
"""

generate_extract = generate_function("extract", extract_param, extract_param, extract_body)

gen_pi_digits = """
def gen_pi_digits():
    z = (1, 0, 0, 1)
    x = gen_x()
    while True:
        y = extract(z, 3)
        while y != extract(z, 4):
            z = compose(z, next(x))
            y = extract(z, 3)
        #z = compose((10, imult(-10, y), 0, 1), z)
        z = compose((10, (-10 * y), 0, 1), z)
        yield y
"""
calc_ndigits_params = ["n"]
calc_ndigits_anns = [":int"]
calc_ndigits_body = """
#def calc_ndigits(n) -> List(Dyn):
    return list(islice(gen_pi_digits(), n))
"""
generate_calc_ndigits = generate_function("calc_ndigits", calc_ndigits_params, calc_ndigits_anns, calc_ndigits_body)

rest = """
def main():
    calc_ndigits(100)

t0 = time.time()
main()
t1 = time.time()
print(t1-t0)
"""
indexList = []
i = 0

compose_start = 0
compose_end = compose_start + len(compose_param)
extract_start = compose_end
extract_end = extract_start + len(extract_param)
calc_ndigits_start = extract_end
calc_ndigits_end = calc_ndigits_start + 1

def copy_range(bit_vector, start, end):
    row = [0] * calc_ndigits_end
    for i in range(start, end):
        row[i] = bit_vector[i]
    return row

def bit_vector_to_bit_matrix(bit_vector):
    copy_curry = lambda start, end: copy_range(bit_vector, start, end)
    gen_x = [0] * calc_ndigits_end
    compose = copy_curry(compose_start, compose_end)
    extract = copy_curry(extract_start, extract_end)
    gen_pi_digits = [0] * calc_ndigits_end
    calc_ndigits = copy_curry(calc_ndigits_start, calc_ndigits_end)
    main = [0] * calc_ndigits_end
    return [gen_x,
            compose,
            extract,
            calc_ndigits,
            main]
def generate_program(bit_vector):
    compose = generate_compose(bit_vector[compose_start:compose_end])
    exract = generate_extract(bit_vector[extract_start:extract_end])
    calc_ndigits = generate_calc_ndigits(bit_vector[calc_ndigits_start:calc_ndigits_end])
    prog = reduce(lambda x, y: x + y, [header,
                                           compose,
                                            exract, gen_pi_digits, calc_ndigits, rest
                                        ], "")
    with open("digits_test.py", "w+") as f:
        f.write(prog)
    cmd = ['retic', '--guarded', 'digits_test.py']
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    time_string = ""
    for line in p.stdout:
        time_string = line
    stripped_time_string = time_string.strip()
    time = float(stripped_time_string)
    print(time)
    p.wait()
    return time
if __name__ == "__main__":
    #generate_program([0] * sieve_end)
    bit_vector = [random.randrange(0,2) for _ in range(calc_ndigits_end)]
    print(bit_vector_to_bit_matrix(bit_vector))
