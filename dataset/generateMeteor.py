import random
import subprocess

callgraph =  [[0, 0, 0, 0, 0, 2, 2, 2, 8], [1, 6, 7, 8, 2, 3, 4, 5, 9]]
def generate_header(function_name, param_names, param_anns, sub_vector):
    """
    String List[String] List[String] Slice[Bit] -> String
    This function accepts a function name, its corresponding parameter names,
    the possible annotations for these parameters, and a slice of the global
    parameter bit vector that corresponds to the parameter indexes.    
    It produces a header with proper type annotations added
    """
    params = [(param_names[index] + param_anns[index] if value else param_names[index]) for index, value in enumerate(sub_vector)]
    param_string = ",".join(params)
    header = "def {0}({1})->Dyn:".format(function_name, param_string)
    return header

def generate_function(function_name, param_names, param_anns, function_body):
    """
    String List[String] List[String] -> (Slice[bit] -> String)
    This function generates a function body with annotations specified by sub_vector
    """
    return (lambda sub_vector: generate_header(function_name, param_names, param_anns, sub_vector) + function_body)


header = """
from __future__ import division, print_function, absolute_import

from bisect import bisect

from six.moves import xrange
import time
#import perf


SOLVE_ARG = 60

WIDTH, HEIGHT = 5, 10
DIR_NO = 6
S, E = WIDTH * HEIGHT, 2
SE = S + (E / 2)
SW = SE - E
W, NW, NE = -E, -SE, -SW

SOLUTIONS = [
    '00001222012661126155865558633348893448934747977799',
    '00001222012771127148774485464855968596835966399333',
    '00001222012771127148774485494855998596835966366333',
    '00001222012771127148774485994855948596835966366333',
    '00001222012771127166773863384653846538445584959999',
    '00001222012771127183778834833348555446554666969999',
    '00001222012771127183778834833348555446554966699996',
    '00001223012331123166423564455647456775887888979999',
    '00001555015541144177484726877268222683336689399993',
    '00001555015541144177484728677286222863338669399993',
    '00001599015591159148594482224827748276837766366333',
    '00001777017871184155845558449984669662932629322333',
    '00004222042664426774996879687759811598315583153331',
    '00004222042774427384773183331866118615586555969999',
    '00004223042334423784523785771855718566186611969999',
    '00004227042874428774528735833155831566316691169999',
    '00004333045534455384175781777812228116286662969999',
    '00004333045934459384559185991866118612286727267772',
    '00004333047734427384277182221866118615586555969999',
    '00004555045514411224172621768277368736683339899998',
    '00004555045514411224172721777299998966836688368333',
    '00004555045534413334132221177266172677886888969999',
    '00004555045534473334739967792617926192661882211888',
    '00004555045564466694699992288828811233317273177731',
    '00004555045564466694699997773172731233312881122888',
    '00004555045564496664999962288828811233317273177731',
    '00004555045564496664999967773172731233312881122888',
    '00004555045584411884171187722866792679236992369333',
    '00004555045584411884191189999832226333267372677766',
    '00004555045584411884191189999866222623336727367773',
    '13335138551389511895778697869947762446624022240000',
    '13777137271333211882888226999946669446554055540000',
    '13777137271333211882888229999649666446554055540000',
    '27776272768221681166819958195548395443954033340000',
    '33322392623926696648994485554855148117871077710000',
    '33366366773867284772842228449584195119551099510000',
    '33366366953869584955849958447784172117721022210000',
    '33366366953869589955849458447784172117721022210000',
    '33386388663866989999277712727142211441554055540000',
    '33396329963297629766822778117148811448554055540000',
    '33399366953869586955846458447784172117721022210000',
    '37776372763332622266899998119148811448554055540000',
    '39999396683336822268277682748477144114551055510000',
    '39999398663338622286277862748477144114551055510000',
    '66777627376233362223899998119148811448554055540000',
    '69999666945564455584333843887738172117721022210000',
    '88811228816629162971629776993743337443554055540000',
    '88822118821333213727137776999946669446554055540000',
    '88822118821333213727137779999649666446554055540000',
    '89999893338663786377286712627142211441554055540000',
    '99777974743984439884333685556855162116621022210000',
    '99995948554483564835648336837766172117721022210000',
    '99996119661366513855133853782547782447824072240000',
    '99996911668166581755817758732548732443324032240000',
    '99996926668261182221877718757148355443554033340000',
    '99996955568551681166812228177248372443774033340000',
    '99996955568551681166813338137748372447724022240000',
    '99996966645564455584333843887738172117721022210000',
    '99996988868877627166277112223143331443554055540000',
    '99997988878857765474655446532466132113321032210000']

"""


rotate_headers = ["def rotate(ido)->List(Dyn):", "def rotate(ido:List(Dyn))->List(Dyn):"]
rotate_params = ["ido"]
rotate_anns = [":List(Dyn)"]
rotate_body = """
    for _ in ido:
        break
    rd={E: NE, NE: NW, NW: W, W: SW, SW: SE, SE: E}
    return [rd[o] for o in ido]

"""
generate_rotate = generate_function("rotate", rotate_params, rotate_anns, rotate_body)


flip_headers = ["def flip(ido)->List(Dyn):", "def flip(ido:List(Dyn))->List(Dyn):"]
flip_params = ["ido"]
flip_anns = [":List(Dyn)"]
flip_body =    """
    for _ in ido:
        break
    fd={E: E, NE: SE, NW: SW, W: W, SW: NW, SE: NE}
    return [fd[o] for o in ido]

"""
generate_flip = generate_function("flip", flip_params, flip_anns, flip_body)


permute_headers = ["def permute(ido, r_ido)->List(Dyn):",
                   "def permute(ido:List(Dyn), r_ido)->List(Dyn):",
                   "def permute(ido, r_ido:List(Dyn))->List(Dyn):",
                   "def permute(ido:List(Dyn), r_ido:List(Dyn))->List(Dyn):"]
permute_params = "ido, r_ido".split(',')
permute_anns = [":List(Dyn)", ":List(Dyn)"]
permute_body = """
    #for _ in ido:
    #    break
    #for _ in r_ido:
    #    break
    #ps = list(ido)
    ps = []
    ps.append(ido)
    for r in xrange(DIR_NO - 1):
        #rotate(list(ido))
    #    for pp in list(ido):
    #        flip(list(ido))
            
        ps.append(rotate(ps[-1]))
        #if ido == r_ido:                 # C2-symmetry
            #ps = ps[0:DIR_NO // 2]
        for i in range(len(ps)):
            pp = ps[i]
            ps.append(flip(pp))
    return ps
    #return []

"""
generate_permute = generate_function("permute", permute_params, permute_anns, permute_body)

convert_headers = ["def convert(ido)->List(Dyn):", "def convert(ido:List(Dyn))->List(Dyn):"]
convert_params = ["ido"]
convert_anns = [":List(Dyn)"]
convert_body = """
    '''incremental direction offsets -> "coordinate offsets" '''
    out = [0.0]
    for o in ido:
        out.append(out[-1] + o)
    return list(set(out))

"""
generate_convert = generate_function("convert", convert_params, convert_anns, convert_body)


get_footprints_headers = ["def get_footprints(board, cti, pieces)->List(List(List(Dyn))):",
                          "def get_footprints(board:List(Dyn), cti, pieces)->List(List(List(Dyn))):",                          
                          "def get_footprints(board, cti, pieces:List(Dyn))->List(List(List(Dyn))):",                          
                          "def get_footprints(board:List(Dyn), cti, pieces:List(Dyn))->List(List(List(Dyn))):"]
get_footprints_params = "board, cti, pieces".split(',')
get_footprints_anns = [":List(Dyn)", ":List(Dyn)", ":List(Dyn)"]
get_footprints_body = """        
    for _ in pieces:
        break
    fps = [[[] for p in xrange(len(pieces))] for ci in xrange(len(board))]
    for c in board:
        for pi, p in enumerate(pieces):
            for pp in p:
                fp = frozenset([cti[c + o] for o in pp if (c + o) in cti])
                if len(fp) == 5:
                    fps[min(fp)][pi].append(fp)
    return fps

"""
generate_get_footprints = generate_function("get_footprints", get_footprints_params, get_footprints_anns, get_footprints_body)

get_senh_headers = ["def get_senh(board, cti)->List(Dyn):",
                    "def get_senh(board:List(Dyn), cti)->List(Dyn):"]
get_senh_params = "board, cti".split(',')
get_senh_anns = [":List(Dyn)", ":List(Dyn)"]
get_senh_body = """
    '''-> south-east neighborhood'''
    se_nh = []
    nh = [E, SW, SE]
    for c in board:
        se_nh.append(frozenset([cti[c + o] for o in nh if (c + o) in cti]))
    return se_nh

"""
generate_get_senh = generate_function("get_senh", get_senh_params, get_senh_anns, get_senh_body)

get_puzzle_headers = ["def get_puzzle(width, height)->Tuple(Dyn, Dyn, List(Dyn)):",
                      "def get_puzzle(width:int, height)->Tuple(Dyn, Dyn, List(Dyn)):",
                      "def get_puzzle(width, height:int)->Tuple(Dyn, Dyn, List(Dyn)):",
                      "def get_puzzle(width:int, height:int)->Tuple(Dyn, Dyn, List(Dyn)):"]
get_puzzle_params = "width, height".split(',')
get_puzzle_anns = [":int", ":int"]
get_puzzle_body = """
    board = [E * x + S * y + (y % 2)
             for y in xrange(height+0)
             for x in xrange(width+0)]
    cti = dict((board[i], i) for i in xrange(len(board)))

    # Incremental direction offsets
    idos = [[E, E, E, SE],
            [SE, SW, W, SW],
            [W, W, SW, SE],
            [E, E, SW, SE],
            [NW, W, NW, SE, SW],
            [E, E, NE, W],
            [NW, NE, NE, W],
            [NE, SE, E, NE],
            [SE, SE, E, SE],
            [E, NW, NW, NW]]

    # Restrict piece 4
    perms = (permute(p, idos[3]) for p in idos)    
    #pieces = [[convert(pp) for pp in p] for p in perms] #this is original code
    p = list(perms) #new code Only due one iteration of previous nested loop from above
    pieces = [[convert(pp) for pp in p[0]]]
    return (board, cti, pieces)
    #return (board, cti, [])

"""
generate_get_puzzle = generate_function("get_puzzle", get_puzzle_params, get_puzzle_anns, get_puzzle_body)

solve_headers = ["def solve(n, i_min, free, curr_board, pieces_left, solutions, fps, se_nh):"]
solve_params = "n, i_min, free, curr_board, pieces_left, solutions, fps, se_nh".split(',')
solve_anns = [":Dyn"] * len(solve_params)
solve_body = """
          # Hack to use a fast local variable to avoid a global lookup

    fp_i_cands = fps[i_min]
    for p in pieces_left:
        fp_cands = fp_i_cands[p]
        for fp in fp_cands:
            if fp <= free:
                n_curr_board = curr_board[:]
                for ci in fp:
                    n_curr_board[ci] = p

                if len(pieces_left) > 1:
                    n_free = free - fp
                    n_i_min = min(n_free)
                    if len(n_free & se_nh[n_i_min]) > 0:
                        n_pieces_left = pieces_left[:]
                        n_pieces_left.remove(p)
                        solve(n, n_i_min, n_free, n_curr_board,
                              n_pieces_left, solutions, fps, se_nh)
                else:
                    s = ''.join(map(str, n_curr_board))
                    solutions.insert(bisect(solutions, s), s)
                    rs = s[::-1]
                    solutions.insert(bisect(solutions, rs), rs)
                    if len(solutions) >= n:
                        return None

        if len(solutions) >= n:
            return None

"""
generate_solve = generate_function("solve", solve_params, solve_anns, solve_body)

bench_meteor_contest_params = "loops, board, pieces, solve_arg, fps, se_nh".split(",")
bench_meteor_contest_anns = [":int", ":List(Dyn)", ":List(Dyn)", ":int", ":List(Dyn)", ":List(Dyn)"]
bench_meteor_contest_body = """
    range_it = xrange(loops)
    #t0 = perf.perf_counter()

    for _ in range_it:
        free = frozenset(xrange(len(board)))
        curr_board = [-1] * len(board)
        pieces_left = list(range(len(pieces)))
        solutions = []
        solve(solve_arg, 0, free, curr_board, pieces_left,
              solutions, fps, se_nh)

    #dt = perf.perf_counter() - t0

    #if solutions != SOLUTIONS:
    #    raise ValueError("unexpected solutions")

    #return dt
    return None
"""
generate_bench_meteor_contest = generate_function("bench_meteor_contest", bench_meteor_contest_params, bench_meteor_contest_anns, bench_meteor_contest_body)

rest = """
def main():
    #runner = perf.Runner()
    #runner.metadata['description'] = "Solver for Meteor Puzzle board"

    board, cti, pieces = get_puzzle(WIDTH, HEIGHT)
    fps = get_footprints(board, cti, pieces)
    se_nh = get_senh(board, cti)

    solve_arg = SOLVE_ARG
    bench_meteor_contest(4, board, pieces, solve_arg, fps, se_nh)
    #runner.bench_time_func('meteor_contest', bench_meteor_contest,
    #                       board, pieces, solve_arg, fps, se_nh)

t0 = time.time()
main()
t1 = time.time()
print(t1-t0)
"""
rotate_start = 0
rotate_end = rotate_start + len(rotate_params)
flip_start = rotate_end
flip_end = flip_start + len(flip_params)
permute_start = flip_end
permute_end = permute_start + len(permute_params)
convert_start = permute_end
convert_end = convert_start + len(convert_params)
get_footprints_start = convert_end
get_footprints_end = get_footprints_start + len(get_footprints_params)
get_senh_start = get_footprints_end
get_senh_end = get_senh_start + len(get_senh_params)
get_puzzle_start = get_senh_end
get_puzzle_end = get_puzzle_start + len(get_puzzle_params)
solve_start = get_puzzle_end
solve_end = solve_start + len(solve_params)
bench_meteor_contest_start = solve_end
bench_meteor_contest_end = bench_meteor_contest_start + len(bench_meteor_contest_params)
#solve_start = get_puzzle_end
#solve_end = solve_start + len(solve_params)

def generate_program(bit_vector):
    #the following lines generate each function definition based on the bitvector's indication of what is annotated
    rotate = generate_rotate(bit_vector[rotate_start:rotate_end])
    flip = generate_flip(bit_vector[flip_start:flip_end])
    permute = generate_permute(bit_vector[permute_start:permute_end])
    convert = generate_convert(bit_vector[convert_start:convert_end])
    get_footprints = generate_get_footprints(bit_vector[get_footprints_start:get_footprints_end])
    get_senh = generate_get_senh(bit_vector[get_senh_start:get_senh_end])
    get_puzzle = generate_get_puzzle(bit_vector[get_puzzle_start:get_puzzle_end])
    solve = generate_solve([0] * len(solve_params))

    #starting here the output file is generated and written to met_test.py
    with open("met_test.py", "w+") as f:
        out = header + rotate + flip + permute + convert + get_footprints + get_senh + get_puzzle + solve + rest
        f.write(out)
    #afterwards we call retic on the generated file
    cmd = ['retic', '--guarded', 'met_test.py']
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    #we parse the returned time and return it
    time_string = ""
    for line in p.stdout:
        time_string = line
    stripped_time_string = time_string.strip()
    time = float(stripped_time_string)
    #print(time)
    p.wait()
    return time

def copy_range(bit_vector, start, end):
    row = [0] * bench_meteor_contest_end
    for i in range(start, end):
        row[i] = bit_vector[i]
    return row

def bit_vector_to_bit_matrix(bit_vector):
    """
    Input: List[int]
    Output: List[List[int]]
    Produce a bit matrix from a bit vector by only getting the bits for parameters
    for a specfic function definition and zeroing out other bits
    """
    curry_help = lambda start, end: copy_range(bit_vector, start, end)
    rotate_row = curry_help(rotate_start, rotate_end)
    flip_row = curry_help(flip_start, flip_end)
    permute_row = curry_help(permute_start, permute_end)
    convert_row = curry_help(convert_start, convert_end)
    get_footprints_row = curry_help(get_footprints_start, get_footprints_end)
    get_senh_row = curry_help(get_senh_start, get_senh_end)
    get_puzzle_row = curry_help(get_puzzle_start, get_puzzle_end)
    bench_meteor_contest_row = curry_help(bench_meteor_contest_start, bench_meteor_contest_end)
    solve_row = [0] * bench_meteor_contest_end
    main_row = [0] * bench_meteor_contest_end
    # now just return the matrix by putting in all of the rows
    return [rotate_row, flip_row, permute_row, convert_row, get_footprints_row, get_senh_row, get_puzzle_row, bench_meteor_contest_row, solve_row, main_row]

def bit_matrix_to_bit_vector(bit_matrix):
    vector_from_matrix = [0] * len(bit_matrix[0])
    for row in bit_matrix:
        for index, el in enumerate(row):
            vector_from_matrix[index] |=  el
    return vector_from_matrix

def main():
    i = 0
    for rotate_index in range(len(rotate_headers)):
        for flip_index in range(len(flip_headers)):
            for permute_index in range(len(permute_headers)):
                for convert_index in range(len(convert_headers)):
                    for get_footprints_index in range(len(get_footprints_headers)):
                        for get_senh_index in range(len(get_senh_headers)):
                            for get_puzzle_index in range(len(get_puzzle_headers)):
                                for solve_index in range(len(solve_headers)):
                                    rotate = rotate_headers[rotate_index] + rotate_body
                                    flip = flip_headers[flip_index] + flip_body
                                    permute = permute_headers[permute_index] + permute_body
                                    convert = convert_headers[convert_index] + convert_body
                                    get_footprints = get_footprints_headers[get_footprints_index] + get_footprints_body
                                    get_senh = get_senh_headers[get_senh_index] + get_senh_body
                                    get_puzzle = get_puzzle_headers[get_puzzle_index] + get_puzzle_body
                                    solve = solve_headers[solve_index] + solve_body
                                    i += 1                                    
                                    with open("met" + str(i) + ".py", "w+") as f:
                                        out = header + rotate + flip + permute + convert + get_footprints + get_senh + get_puzzle + solve + rest
                                        f.write(out)
                                    
                                    if i > 1024:
                                        return

if __name__ == "__main__":
    #generate_program([1,1,1,1,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
    test_vector = [1,1,1,1,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    test_matrix = bit_vector_to_bit_matrix(test_vector)
    #print(len(test_matrix))
    #print(bit_matrix_to_bit_vector(test_matrix) == test_vector)
