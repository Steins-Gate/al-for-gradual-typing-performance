import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import os
PATH = os.getcwd()

def raytrace(clf = False):
    X, y = pd.read_csv(os.path.join(PATH, "dataset", "raytrace_fea.csv")).to_numpy(), \
           pd.read_csv(os.path.join(PATH, "dataset", "raytrace_time.csv")).to_numpy().reshape(-1,1)
           
    if clf == True:
        p1, p2 = np.percentile(y, 33), np.percentile(y, 66)
        y[y<p1], y[(y>=p1) & (y<=p2)], y[y>p2]  = 0, 1, 2
    return X, y

def meteor(clf = False):
    X, y = pd.read_csv(os.path.join(PATH, "dataset", "meteor_bits.txt"),sep=" ", header=None).to_numpy(), \
           pd.read_csv(os.path.join(PATH, "dataset", "meteor_log.txt"),sep=" ", header=None).to_numpy()
    X_arr, y_arr = [], []
    for i, (xi, yi) in enumerate(zip(X, y)):
        if i % 2 == 0 and xi == yi:
            pass
        else:
            xi = np.char.split(X[i].astype(str), sep =',')
            xi = np.array(xi[0]).astype(np.int)
            X_arr.append(xi)
            y_arr.append(float(y[i][0]))
    X, y = np.array(X_arr), np.array(y_arr).reshape(-1, 1)
    if clf == True:
        p1, p2 = np.percentile(y, 33), np.percentile(y, 66)
        y[y<p1], y[(y>=p1) & (y<=p2)], y[y>p2]  = 0, 1, 2
    return X, y

def nbody(clf = False):
    X, y = pd.read_csv(os.path.join(PATH, "dataset", 'nbody_bits.txt'), sep=" ", header=None).to_numpy(), \
           pd.read_csv(os.path.join(PATH, "dataset", 'nbody_log.txt'), sep=" ", header=None).to_numpy()
    X_arr, y_arr = [], []
    for i, (xi, yi) in enumerate(zip(X, y)):
        if i % 2 == 0 and xi == yi:
            pass
        else:
            xi = np.char.split(X[i].astype(str), sep=',')
            xi = np.array(xi[0]).astype(np.int)
            X_arr.append(xi)
            y_arr.append(float(y[i][0]))
    X, y = np.array(X_arr), np.array(y_arr).reshape(-1, 1)
    if clf == True:
        p1, p2 = np.percentile(y, 33), np.percentile(y, 66)
        y[y < p1], y[(y >= p1) & (y <= p2)], y[y > p2] = 0, 1, 2
    return X, y


def sor(clf = False):
    X, y = pd.read_csv(os.path.join(PATH, "dataset", "SOR_bits.txt"),sep=" ", header=None).to_numpy(), \
           pd.read_csv(os.path.join(PATH, "dataset", "SOR_log.txt"),sep=" ", header=None).to_numpy()
    X_arr, y_arr = [], []
    for i, (xi, yi) in enumerate(zip(X, y)):
        if i % 2 == 0 and xi == yi:
            pass
        else:
            xi = np.char.split(X[i].astype(str), sep =',')
            xi = np.array(xi[0]).astype(np.int)
            X_arr.append(xi)
            y_arr.append(float(y[i][0]))
    X, y = np.array(X_arr), np.array(y_arr).reshape(-1, 1)
    if clf == True:
        p1, p2 = np.percentile(y, 33), np.percentile(y, 66)
        y[y<p1], y[(y>=p1) & (y<=p2)], y[y>p2]  = 0, 1, 2
    return X, y
def suffixtree(clf = False):
    temp_holder=pd.read_csv(os.path.join(PATH, "dataset", 'suffixtree_bits.txt'), sep=" ", header=None).to_numpy()
    X_arr, y_arr = [], []
    for i, xi in enumerate(temp_holder):
        if i%2==1:
            y_arr.append(float(temp_holder[i][0]))
        else:
            xi = np.char.split(temp_holder[i].astype(str), sep=',')
            xi = np.array(xi[0]).astype(np.int)
            X_arr.append(xi)
    X,y=np.array(X_arr), np.array(y_arr).reshape(-1, 1)
    if clf == True:
        p1, p2 = np.percentile(y, 33), np.percentile(y, 66)
        y[y < p1], y[(y >= p1) & (y <= p2)], y[y > p2] = 0, 1, 2
    return X, y

def synth(clf = False):
    temp_holder=pd.read_csv(os.path.join(PATH, "dataset", 'synth_bits.txt'), sep=" ", header=None).to_numpy()
    X_arr, y_arr = [], []
    for i, xi in enumerate(temp_holder):
        if i%2==1:
            y_arr.append(float(temp_holder[i][0]))
        else:
            xi = np.char.split(temp_holder[i].astype(str), sep=',')
            xi = np.array(xi[0]).astype(np.int)
            X_arr.append(xi)
    X,y=np.array(X_arr), np.array(y_arr).reshape(-1, 1)
    if clf == True:
        p1, p2 = np.percentile(y, 33), np.percentile(y, 66)
        y[y < p1], y[(y >= p1) & (y <= p2)], y[y > p2] = 0, 1, 2
    return X, y
def gregor(clf = False):
    temp_holder=pd.read_csv(os.path.join(PATH, "dataset", 'gregor_bits.txt'), sep=" ", header=None).to_numpy()

    X_arr, y_arr = [], []
    for i, xi in enumerate(temp_holder):
        if i%2==1:
            y_arr.append(float(temp_holder[i][0]))
        else:
            xi = np.char.split(temp_holder[i].astype(str), sep=',')
            xi = np.array(xi[0]).astype(np.int)
            X_arr.append(xi)
            
    X,y=np.array(X_arr), np.array(y_arr).reshape(-1, 1)
    if clf == True:
        p1, p2 = np.percentile(y, 33), np.percentile(y, 66)
        y[y < p1], y[(y >= p1) & (y <= p2)], y[y > p2] = 0, 1, 2
    return X, y
def quad(clf = False):
   
    temp_holder=pd.read_csv(os.path.join(PATH, "dataset", 'quad_bits.txt'), sep=" ", header=None).to_numpy()

    X_arr, y_arr = [], []
    for i, xi in enumerate(temp_holder):
        if i%2==1:
            
            y_arr.append(float(temp_holder[i][0]))
        else:
            xi = np.char.split(temp_holder[i].astype(str), sep=',')
            xi = np.array(xi[0]).astype(np.int)
            X_arr.append(xi)
            
    X,y=np.array(X_arr), np.array(y_arr).reshape(-1, 1)
    if clf == True:
        p1, p2 = np.percentile(y, 33), np.percentile(y, 66)
        y[y < p1], y[(y >= p1) & (y <= p2)], y[y > p2] = 0, 1, 2
    return X, y
def tetris(clf=False):
    X_arr, y_arr = [], []
    with open(os.path.join(PATH, "dataset", 'tetris_bits.txt')) as fp:
        line = fp.readline()
        cnt = 1
        while line:
            if cnt%2==0:
                y_arr.append(float(line.strip()))
            else:
                X_arr.append(list(map(int,list(line.strip()))))
           
            line = fp.readline()
            cnt += 1
            if cnt>1024:
                break
    X,y=np.array(X_arr), np.array(y_arr).reshape(-1, 1)
    return X, y
def inn_bits(clf=False):
    X_arr, y_arr = [], []
    
    with open(os.path.join(PATH, "dataset", 'lnm_bits.txt')) as fp:
        line = fp.readline()
        cnt = 1
        while line:
            if cnt%2==0:
                y_arr.append(float(line.strip()))
            else:
                X_arr.append(list(map(int,list(line.strip()))))
           
            line = fp.readline()
            cnt += 1
            if cnt>128:
                break
    X,y=np.array(X_arr), np.array(y_arr).reshape(-1, 1)
    return X, y
if __name__ == "__main__":
    #print(PATH)
    
    X, y = sor()
    print(X)
    print(y)