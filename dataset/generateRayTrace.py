import random
import pdb
import subprocess
callgraph =  [[34, 33, 33, 33, 33, 33, 33, 33, 33, 33, 22, 7, 7, 2], [33, 22, 0, 26, 24, 11, 32, 25, 7, 5, 0, 2, 5, 1]]
def generate_header(function_name, param_names, param_anns, sub_vector):
    """
    String List[String] List[String] Slice[Bit] -> String
    This function accepts a function name, its corresponding parameter names,
    the possible annotations for these parameters, and a slice of the global
    parameter bit vector that corresponds to the parameter indexes.    
    It produces a header with proper type annotations added
    """
    if len(param_names) != len(sub_vector):
        pdb.set_trace()
    params = [(param_names[index] + param_anns[index] if value else param_names[index]) for index, value in enumerate(sub_vector)]
    param_string = ",".join(params)
    header = "def {0}({1})->Dyn:".format(function_name, param_string)
    return header

def generate_function(function_name, param_names, param_anns, function_body):
    """
    String List[String] List[String] -> (Slice[bit] -> String)
    This function generates a function body with annotations specified by sub_vector
    """
    return (lambda sub_vector: generate_header(function_name, param_names, param_anns, sub_vector) + function_body)


header = """

import array
import math
import time
#import perf
import pdb
from six.moves import xrange


DEFAULT_WIDTH = 100
DEFAULT_HEIGHT = 100
EPSILON = 0.00001
ZERO = (0, 0, 0)#vector(0, 0, 0)
RIGHT = (1, 0, 0)#vector(1, 0, 0)
UP = (0, 1, 0)#vector(0, 1, 0)
OUT = (0, 0, 1)#vector(0, 0, 1)

"""

vector_headers = ["def vector(initx, inity, initz)->Tuple(Dyn,Dyn,Dyn):"]
vector_params = "initx, inity, initz".split(',')
vector_anns = [":float", ":float", ":float"]
vector_body = """
    return (initx, inity, initz)

"""
generate_vector = generate_function("vector", vector_params, vector_anns, vector_body)

    #def vectorstr(vec):
    #    (x,y,z) = vec
    #    return '(%s,%s,%s)' % (x, y, z)

    #def __repr__(vec):
    #    (x,y,z) = vec
    #    return 'Vector(%s,%s,%s)' % (x, y, z)

dot_headers = ["def dot(vec, other)->float:","def dot(vec:Tuple(Dyn,Dyn,Dyn), other)->float:","def dot(vec, other:Tuple(Dyn,Dyn,Dyn))->float:","def dot(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))->float:"]
dot_params = "vec, other".split(',')
dot_anns = [":Tuple(float,float,float)", ":Tuple(float,float,float)"]
dot_body = """
    #other.mustBeVector()
    (x,y,z) = vec
    (x1, y1, z1) = other
    return (x * x1 * 1.0) + (y * y1 * 1.0) + (z * z1 * 1.0)

"""
generate_dot = generate_function("dot", dot_params, dot_anns, dot_body)

magnitude_headers = ["def magnitude(vec)->float:","def magnitude(vec:Tuple(Dyn,Dyn,Dyn))->float:"]
magnitude_params = ["vec"]
magnitude_anns = [":Tuple(float,float,float)"]
magnitude_body = """
    return (math.sqrt(dot(vec, vec)) + 0.0)

"""
generate_magnitude = generate_function("magnitude", magnitude_params, magnitude_anns, magnitude_body)

add_headers = ["def add(vec, other)-> Tuple(Dyn,Dyn,Dyn):","def add(vec:Tuple(Dyn,Dyn,Dyn), other)-> Tuple(Dyn,Dyn,Dyn):", "def add(vec, other:Tuple(Dyn,Dyn,Dyn))-> Tuple(Dyn,Dyn,Dyn):", "def add(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))-> Tuple(Dyn,Dyn,Dyn):"]
add_params = "vec, other".split(',')
add_anns = [":Tuple(float,float,float)", ":Tuple(float,float,float)"]

add_body = """
    (x,y,z) = vec
    (x1,y1,z1) = other
    return (x+x1, y+y1, z+z1)
    #if other.isPoint():
        #return Point(self.x + other.x, self.y + other.y, self.z + other.z)
    #else:
        #return Vector(self.x + other.x, self.y + other.y, self.z + other.z)

"""
generate_add = generate_function("add", add_params, add_anns, add_body)
    
sub_headers = ["def sub(vec, other)->Tuple(Dyn,Dyn,Dyn):","def sub(vec:Tuple(Dyn,Dyn,Dyn), other)->Tuple(Dyn,Dyn,Dyn):","def sub(vec, other:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):","def sub(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):"]
sub_params = "vec, other".split(',')
sub_anns = [":Tuple(float,float,float)", ":Tuple(float,float,float)"]
sub_body = """
    #other.mustBeVector()
    (x,y,z) = vec
    (x1,y1,z1) = other
    return vector(x - x1, y - y1, z - z1)

"""
generate_sub = generate_function("sub", sub_params, sub_anns, sub_body)

scale_headers = ["def scale(vec, factor)->Tuple(Dyn,Dyn,Dyn):","def scale(vec:Tuple(Dyn,Dyn,Dyn), factor)->Tuple(Dyn,Dyn,Dyn):"]
scale_params = "vec, factor".split(',')
scale_anns = [":Tuple(float,float,float)", ":float"]

scale_body = """
    x,y,z = vec
    return (factor * x, factor * y, factor * z)

"""
generate_scale = generate_function("scale", scale_params, scale_anns, scale_body)

    
cross_headers = ["def cross(vec, other)->Tuple(Dyn,Dyn,Dyn):","def cross(vec:Tuple(Dyn,Dyn,Dyn), other)->Tuple(Dyn,Dyn,Dyn):","def cross(vec, other:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):","def cross(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):"]
cross_params = "vec, other".split(',')
cross_anns = [":Tuple(float,float,float)", ":Tuple(float,float,float)"]
cross_body = """
    #other.mustBeVector()
    (x,y,z) = vec
    (x1,y1,z1) = other
    return (y * z1 - z * y1,
                      z * x1 - x * z1,
                      x * y1 - y * x1)

"""
generate_cross = generate_function("cross", cross_params, cross_anns, cross_body)

normalized_headers = ["def normalized(vec)-> Tuple(Dyn,Dyn,Dyn):","def normalized(vec:Tuple(Dyn,Dyn,Dyn))-> Tuple(Dyn,Dyn,Dyn):"]
normalized_params = ["vec"]
normalized_anns = [":Tuple(float,float,float))"]
normalized_body = """
    return scale(vec, 1.0 / magnitude(vec))

"""
generate_normalized = generate_function("normalized", normalized_params, normalized_anns, normalized_body)

negated_headers = ["def negated(vec)->Tuple(Dyn,Dyn,Dyn):","def negated(vec:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):"]
negated_params = ["vec"]
negated_anns = [":Tuple(float,float,float)"]
negated_body = """
    return scale(vec, -1)

"""
generate_negated = generate_function("negated", negated_params, negated_anns, negated_body)

eq_headers = ["def eq(vec, other)->Bool:","def eq(vec:Tuple(Dyn,Dyn,Dyn), other)->Bool:","def eq(vec, other:Tuple(Dyn,Dyn,Dyn))->Bool:","def eq(vec, other):","def eq(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))->Bool:"]
eq_params = "vec, other".split(',')
eq_anns = [":Tuple(float,float,float)", ":Tuple(float,float,float)"]
eq_body = """
    x,y,z = vec
    x1,y1,z1 = other
    return (x == x1) and (y == y1) and (z == z1)

"""
generate_eq = generate_function("eq", eq_params, eq_anns, eq_body)

    #def isVector(self):
    #    return True

    #def isPoint(self):
    #    return False

    #def mustBeVector(self):
    #    return self

    #def mustBePoint(self):
    #    raise 'Vectors are not points!'

reflectThrough_headers = ["def reflectThrough(vec, normal)->Tuple(Dyn,Dyn,Dyn):",
                              "def reflectThrough(vec:Tuple(Dyn,Dyn,Dyn), normal)->Tuple(Dyn,Dyn,Dyn):",
                              "def reflectThrough(vec, normal:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):",
                              "def reflectThrough(vec:Tuple(Dyn,Dyn,Dyn), normal:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):"]
reflectThrough_params = "vec, normal".split(',')
reflectThrough_anns = [":Tuple(float,float,float)", ":Tuple(float,float,float)"]
reflectThrough_body = """
    d = scale(normal, dot(vec, normal))
    return sub(vec, scale(d,2))

"""
generate_reflectThrough = generate_function("reflectThrough", reflectThrough_params, reflectThrough_anns, reflectThrough_body)



    #assert Vector.RIGHT.reflectThrough(Vector.UP) == Vector.RIGHT
    #assert Vector(-1, -1, 0).reflectThrough(Vector.UP) == Vector(-1, 1, 0)


    #class Point(object):

    #    def __init__(self, initx, inity, initz):
    #        self.x = initx
    #        self.y = inity
    #        self.z = initz

    #    def __str__(self):
    #        return '(%s,%s,%s)' % (self.x, self.y, self.z)

    #    def __repr__(self):
    #        return 'Point(%s,%s,%s)' % (self.x, self.y, self.z)

    #    def __add__(self, other):
    #        other.mustBeVector()
    #        return Point(self.x + other.x, self.y + other.y, self.z + other.z)

    #    def __sub__(self, other):
    #        if other.isPoint():
    #            return Vector(self.x - other.x, self.y - other.y, self.z - other.z)
    #        else:
    #            return Point(self.x - other.x, self.y - other.y, self.z - other.z)

    #    def isVector(self):
    #        return False

    #    def isPoint(self):
    #        return True

    #    def mustBeVector(self):
    #        raise 'Points are not vectors!'

    #    def mustBePoint(self):
    #        return self


    #class Sphere(object):

sphere_headers = ["def sphere(centre, radius)->Tuple(Dyn,Dyn):"]
sphere_params = "centre, radius".split(',')
sphere_anns = [":Tuple(float,float,float)", ":float"]
sphere_body = """
    #centre.mustBePoint()
    #self.centre = centre
    #self.radius = radius
    return (centre, radius)
"""
generate_sphere = generate_function("sphere", sphere_params, sphere_anns, sphere_body)

    #def __repr__(self):
    #    return 'Sphere(%s,%s)' % (repr(self.centre), self.radius)

    #class Ray(object):

ray_headers = ["def ray(point, vect):","def ray(point, vect:Tuple(Dyn,Dyn,Dyn)):"]
ray_params = "point, vect".split(',')
ray_anns = [":Tuple(float,float,float)", ":Tuple(float,float,float)"]
ray_body = """
    #self.point = point
    #self.vector = vector.normalized()
    return (point, normalized(vect))

"""
generate_ray = generate_function("ray", ray_params, ray_anns, ray_body)

    #def __repr__(self):
    #    return 'Ray(%s,%s)' % (repr(self.point), repr(self.vector))

pointAtTime_headers = ["def pointAtTime(ray, t)->Tuple(Dyn,Dyn,Dyn):"]
pointAtTime_params = "ray, t".split(',')
pointAtTime_anns = [":Tuple(Tuple(float,float,float),Tuple(float,float,float))", ":float"]
pointAtTime_body = """
    point, vector = ray
    return add(point, scale(vector, t))

"""
generate_pointAtTime = generate_function("pointAtTime", pointAtTime_params, pointAtTime_anns, pointAtTime_body)

intersectionTime_headers = ["def intersectionTime(s, ra):",
                            "def intersectionTime(s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn), ra):",
                            "def intersectionTime(s, ra:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn))):",
                            "def intersectionTime(s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn), ra:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn))):"]
intersectionTime_params = "s, ra".split(',')
intersectionTime_anns = [":Tuple(Tuple(float,float,float),float)", ":Tuple(Tuple(float,float,float),Tuple(float,float,float))"]
intersectionTime_body = """
    (centre, radius) = s
    (point, vect) = ra
    cp = sub(centre, point)
    v = dot(cp, vect)
    discriminant = (radius * radius) - (dot(cp, cp) - v * v)
    if discriminant < 0:
        return None
    else:
        return v - math.sqrt(discriminant)

"""
generate_intersectionTime = generate_function("intersectionTime", intersectionTime_params, intersectionTime_anns, intersectionTime_body)

normalAt_headers = ["def normalAt(s, p)->Tuple(Dyn,Dyn,Dyn):",
                    "def normalAt(s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn), p)->Tuple(Dyn,Dyn,Dyn):",
                    "def normalAt(s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn), p:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):"]
normalAt_params = "s, p".split(',')
normalAt_anns = [":Tuple(Tuple(float,float,float),float)", ":Tuple(float,float,float)"]
normalAt_body = """
    (centre, radius) = s
    return normalized((sub(p,  centre)))

"""
generate_normalAt = generate_function("normalAt", normalAt_params, normalAt_anns, normalAt_body)


    #class Halfspace(object):

halfspace_headers = ["def halfspace(point, normal)->Tuple(Dyn, Tuple(Dyn,Dyn,Dyn)):",
                     "def halfspace(point, normal:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn, Tuple(Dyn,Dyn,Dyn)):"]
halfspace_params = "point, normal".split(',')
halfspace_anns = [":Tuple(float,float,float)", ":Tuple(float,float,float)"]
halfspace_body = """
    #self.point = point
    #self.normal = normal.normalized()
    return (point, normalized(normal))
"""
generate_halfspace = generate_function("halfspace", halfspace_params, halfspace_anns, halfspace_body)

    #def __repr__(self):
    #    return 'Halfspace(%s,%s)' % (repr(self.point), repr(self.normal))

intersectionTime1_headers = ["def intersectionTime1(hs, ray)->float:",
                             "def intersectionTime1(hs:Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)), ray)->float:",
                             "def intersectionTime1(hs, ray:Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))->float:",
                             "def intersectionTime1(hs:Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)), ray:Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))->float:"]
intersectionTime1_params = "hs, ray".split(',')
intersectionTime1_anns = [":Tuple(Tuple(float,float,float),Tuple(float,float,float)", ":Tuple(Tuple(float,float,float),Tuple(float,float,float))"]
intersectionTime1_body = """
    point, vector = ray
    (point, normal) = hs
    v = dot(vector, normal)
    if v:
        return 1 / -v
    return -1.0

"""
generate_intersectionTime1 = generate_function("intersectionTime1", intersectionTime1_params, intersectionTime1_anns, intersectionTime1_body)

normalAt1_headers = ["def normalAt1(hs, p):",
                     "def normalAt1(hs:Tuple(Dyn,Dyn), p):"]
normalAt1_params = "hs, p".split(',')
normalAt1_anns = [":Tuple(Tuple(float,float,float),float,float,float)", ":Tuple(float,float,float)"]
normalAt1_body = """
    (p1, normal) = hs
    return normal

"""
generate_normalAt1 = generate_function("normalAt1", normalAt1_params, normalAt1_anns, normalAt1_body)




    #Point.ZERO = Point(0, 0, 0)


    #class Canvas(object):

canvas_headers = ["def canvas(width, height)->Tuple(Dyn,Dyn,int):"]
canvas_params = "width, height".split(',')
canvas_anns = [":int", ":int"]
canvas_body = """
    byts = array.array('B', [0] * (width * height * 3))
    #byts = [0] * (width * height * 3)
    for i in xrange(width * height):
        #0
        byts[i * 3 + 2] = 255
    #self.width = width
    #self.height = height
    return (byts, width, height)

"""
generate_canvas = generate_function("canvas", canvas_params, canvas_anns, canvas_body)

plot_headers = ["def plot(canv, x, y, r, g, b):",
                "def plot(canv:Tuple(Dyn,Dyn,Dyn), x, y, r, g, b):",
                "def plot(canv, x:int, y, r, g, b):",
                "def plot(canv, x, y:int, r, g, b):",
                "def plot(canv, x, y, r:int, g, b):",
                "def plot(canv, x, y, r, g:int, b):",
                "def plot(canv, x, y, r, g, b:int):",
                "def plot(canv:Tuple(Dyn,Dyn,Dyn), x:int, y:int, r:int, g:int, b:int):"]
plot_params = "canv, x, y, r, g, b".split(',')
plot_anns = [":Tuple(int,int,int)", ":int", ":int", ":int", ":int", ":int"]
plot_body = """
    (byts, width, height) = canv
    i = ((height - y - 1) * width + x) * 3
    byts[i] = max(0, min(255, int(r * 255)))
    byts[i + 1] = max(0, min(255, int(g * 255)))
    byts[i + 2] = max(0, min(255, int(b * 255)))
    return None

"""
generate_plot = generate_function("plot", plot_params, plot_anns, plot_body)

    #def write_ppm(self, filename):
    #    header = 'P6 %d %d 255\n' % (self.width, self.height)
    #    with open(filename, "wb") as fp:
    #        fp.write(header.encode('ascii'))
    #        fp.write(self.bytes.tostring())

firstIntersection_headers = ["def firstIntersection(intersections):",
                             "def firstIntersection(intersections:List(Dyn)):"]
firstIntersection_params = ["intersections"]
firstIntersection_anns = [":List(Dyn)"]
firstIntersection_body = """
    result = None
    for i in intersections:
        candidateT = i[1]
        if candidateT is not None and candidateT > -EPSILON:
            if result is None or candidateT < result[1]:
                result = i
    return result

"""
generate_firstIntersection = generate_function("firstIntersection", firstIntersection_params, firstIntersection_anns, firstIntersection_body)


    #class Scene(object):
scene_headers = ["def scene()->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Int):"]
scene_params = ""
scene_anns = ""
scene_body = """
    objects = []
    lightPoints = []
    position = vector(0.0, 1.8, 10.0)
    lookingAt = (0.0,0.0,0.0)#Point.ZERO
    fieldOfView = 45
    recursionDepth = 0
    return (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

"""
generate_scene = generate_function("scene", scene_params, scene_anns, scene_body)

moveTo_headers = ["def moveTo(sc, p)->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):",
                  "def moveTo(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), p)->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):"]
moveTo_params = "sc, p".split(',')
moveTo_anns = [":Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)", ":Tuple(float,float,float)"]
moveTo_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    position = p
    return (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

"""
generate_moveTo = generate_function("moveTo", moveTo_params, moveTo_anns, moveTo_body)

lookAt_headers = ["def lookAt(sc, p):","def lookAt(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), p)->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):"]
lookAt_params = "sc, p".split(',')
lookAt_anns = [":Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)", ":Tuple(float,float,float)"]
lookAt_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    lookingAt = p
    return (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

"""
generate_lookAt = generate_function("lookAt", lookAt_params, lookAt_anns, lookAt_body)

addObject_headers = ["def addObject(sc, object, surface):","def addObject(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), object, surface)-> Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):"]
addObject_params = "sc, object, surface".split(',')
addObject_anns = [":Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)", ":Tuple(Dyn,Dyn,Dyn,Dyn)"]
addObject_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    objs = [] + objects
    objs.append((object, surface))
    return (objs, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

"""
generate_addObject = generate_function("addObject", addObject_params, addObject_anns, addObject_body)

addLight_headers = ["def addLight(sc, p):","def addLight(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), p)->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):"]
addLight_params = "sc, p".split(',')
addLight_anns = [":Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)", ":Tuple(int,int,int)"]
addLight_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    lps = [] + lightPoints
    lps.append(p)
    return (objects, lps, position, lookingAt, fieldOfView, recursionDepth)

"""
generate_addLight = generate_function("addLight", addLight_params, addLight_anns, addLight_body)

addColours_headers = ["def addColours(a, scale, b)->Tuple(Dyn,Dyn,Dyn):"]
addColours_params = "a, scale, b".split(',')
addColours_anns = [":List(float)", ":float", ":List(float)"]
addColours_body = """
    return (a[0] + scale * b[0],
            a[1] + scale * b[1],
            a[2] + scale * b[2])

"""
generate_addColours = generate_function("addColours", addColours_params, addColours_anns, addColours_body)

baseColourAt_headers = ["def baseColourAt(ss, p):","def baseColourAt(ss:Tuple(Dyn,Dyn,Dyn,Dyn), p):"]
baseColourAt_params = "ss, p".split(',')
baseColourAt_anns = [":Tuple(Dyn,Dyn,Dyn,Dyn)", ":Tuple(float,float,float)"]
baseColourAt_body = """
    (baseColour, specularCoefficient, lambertCoefficient, ambientCoefficient) = ss
    return baseColour

"""
generate_baseColourAt = generate_function("baseColourAt", baseColourAt_params, baseColourAt_anns, baseColourAt_body)

colourAt_headers = ["def colourAt(ss, scene1, ray1, p1, normal1):",
                    "def colourAt(ss, scene1, ray1, p1:Tuple(Dyn,Dyn,Dyn), normal1):",
                    "def colourAt(ss, scene1, ray1, p1, normal1:Tuple(Dyn,Dyn,Dyn)):"
                    "def colourAt(ss:Tuple(Dyn,Dyn,Dyn,Dyn), scene1, ray1, p1, normal1):",
                    "def colourAt(ss, scene1, ray1:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn)), p1, normal1):",
                    "def colourAt(ss:Tuple(Dyn,Dyn,Dyn,Dyn), scene1, ray1:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn)), p1, normal1):",
                    "def colourAt(ss:Tuple(Dyn,Dyn,Dyn,Dyn), scene1, ray1:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn)), p1, normal1:Tuple(Dyn,Dyn,Dyn)):"]
colourAt_params = "ss, scene1, ray1, p1, normal1".split(',')
colourAt_anns = [":Tuple(Dyn,Dyn,Dyn,Dyn)", ":Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)", ":Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(float,float,float)", ":Tuple(float,float,float)"]
colourAt_body = """
    (baseColour, specularCoefficient, lambertCoefficient, ambientCoefficient)
    b = baseColourAt(ss, p)
    (p,v) = ray1

    c = (0, 0, 0)
    if specularCoefficient > 0:
        reflectedRay = ray(p1, reflectThrough(v, vnormal))
        reflectedColour = rayColour(scene1,reflectedRay)
        c = addColours(c, specularCoefficient, reflectedColour)

    if lambertCoefficient > 0:
        lambertAmount = 0
        for lightPoint in visibleLights(scene1, p1):
            contribution = dot(normalized((sub(lightPoint, p1))), normal)
            if contribution > 0:
                lambertAmount = lambertAmount + contribution
        lambertAmount = min(1, lambertAmount)
        c = addColours(c, lambertCoefficient * lambertAmount, b)

    if ambientCoefficient > 0:
        c = addColours(c, ambientCoefficient, b)

    return c

"""
generate_colourAt = generate_function("colourAt", colourAt_params, colourAt_anns, colourAt_body)


rayColour_headers = ["def rayColour(sc, ry):",
                         "def rayColour(sc, ry:Tuple(Dyn,Dyn,Dyn)):"]
rayColour_params = "sc, ry".split(',')
rayColour_anns = [":Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)", ":Tuple(float,float,float)"]
rayColour_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    if recursionDepth > 3:
        return (0.0, 0.0, 0.0)
    try:
        recursionDepth = recursionDepth + 1
        intersections = [(o, intersectionTime(o,ry), s)
                             for (o, s) in objects]
        i = firstIntersection(intersections)
        if i is None:
            return (0.0, 0.0, 0.0)  # the background colour
        else:
            (o, t, s) = i
            p = pointAtTime(ry, t)
            return colourAt(s, sc, ry, p, o.normalAt(p))
    finally:
        recursionDepth = recursionDepth - 1
    return (0.0,0.0,0.0)

"""
generate_rayColour = generate_function("rayColour", rayColour_params, rayColour_anns, rayColour_body)

render_headers = ["def render(sc, canvas1):",
                  "def render(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), canvas1):"]
render_params = "sc, canvas1".split(',')
render_anns = [":Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)", ":Tuple(Dyn,Dyn,Dyn)"]
render_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    fovRadians = math.pi * (fieldOfView / 2.0) / 180.0
    halfWidth = math.tan(fovRadians)
    halfHeight = 0.75 * halfWidth
    width = halfWidth * 2
    height = halfHeight * 2
    bytes, w, h = canvas1
    pixelWidth = width / (w - 1)
    pixelHeight = height / (h - 1)

    eye = ray(position, sub(lookingAt, position))
    p, v = eye    
    vpRight = normalized(cross(v, UP))
    vpUp = normalized(cross(vpRight, v))

    for y in xrange(int(height)):
        for x in xrange(int(width)):
            xcomp = scale(vpRight, x * pixelWidth - halfWidth)
            ycomp = scale(vpUp, y * pixelHeight - halfHeight)
            r = ray(p, add(add(v, xcomp), ycomp))
            #colour = rayColour(sc, r)
            #plot(canvas1, x, y, *colour)

    return None

"""
generate_render = generate_function("render", render_params, render_anns, render_body)    

lightIsVisible_headers = ["def lightIsVisible(sc, l, p):",
                          "def lightIsVisible(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), l, p):"]
lightIsVisible_params = "sc, l, p".split(',')
lightIsVisible_anns = [":Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)", ":Tuple(float,float,float)", ":Tuple(float,float,float)"]
lightIsVisible_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    for (o, s) in objects:
        t = intersectionTime(o, ray(p, sub(l, p)))
        if t is not None and t > EPSILON:
            return False
    return True

"""
generate_lightIsVisible = generate_function("lightIsVisible", lightIsVisible_params, lightIsVisible_anns, lightIsVisible_body)
    
visibleLights_headers = ["def visibleLights(sc, p):","def visibleLights(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), p):"]
visibleLights_params = "sc, p".split(',')
visibleLights_anns = [":Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)", ":Tuple(float,float,float)"]
visibleLights_body = """
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    result = []
    for l in lightPoints:
        if lightIsVisible(sc, l, p):
            result.append(l)
    return result

"""
generate_visibleLights = generate_function("visibleLights", visibleLights_params, visibleLights_anns, visibleLights_body)




    #class SimpleSurface(object):

simpleSurface_headers = ["def simpleSurface(baseColour)->Tuple(Dyn,Float,Float,Dyn):"]
simpleSurface_params = ["baseColour"]
simpleSurface_anns = ":int"
simpleSurface_body = """
    #baseColour = kwargs.get('baseColour', (1, 1, 1))
    specularCoefficient = 0.2
    lambertCoefficient =  0.6
    ambientCoefficient = 1.0 - specularCoefficient - lambertCoefficient
    return (baseColour, specularCoefficient, lambertCoefficient, ambientCoefficient)

"""
generate_simpleSurface = generate_function("simpleSurface", simpleSurface_params, simpleSurface_anns, simpleSurface_body)


    #class CheckerboardSurface(SimpleSurface):

    #    def __init__(self, **kwargs):
    #        SimpleSurface.__init__(self, **kwargs)
    #        self.otherColour = kwargs.get('otherColour', (0, 0, 0))
    #        self.checkSize = kwargs.get('checkSize', 1)

    #    def baseColourAt(self, p):
    #        v = p - Point.ZERO
    #        v.scale(1.0 / self.checkSize)
    #        if (int(abs(v.x) + 0.5) +
    #            int(abs(v.y) + 0.5) +
    #            int(abs(v.z) + 0.5)) \
        #           % 2:
    #            return self.otherColour
    #        else:
    #            return self.baseColour

bench_raytrace_headers = ["def bench_raytrace(loops, width, height, filename):"]
bench_raytrace_params = "loops, width, height, filename".split(',')
bench_raytrace_anns = [":int", ":int", ":int", ":string"]
bench_raytrace_body = """
    range_it = xrange(loops)
    #t0 = perf.perf_counter()

    for i in range_it:
        #canvas1 = canvas(width, height)
        s = scene()
        addLight(s, vector(30, 30, 10))
        addLight(s, vector(-10, 100, 30))
        lookAt(s, vector(0, 3, 0))
        addObject(s, sphere(vector(1, 3, -10), 2),
                    simpleSurface((1, 1, 0)))
        for y in xrange(6):
            addObject(s,sphere(vector(-3 - y * 0.4, 2.3, -5), 0.4),
                        simpleSurface((y / 6.0, 1 - y / 6.0, 0.5)))
            scale(normalized(vector(10,23,19)), y * 11)
        #s.addObject(Halfspace(Point(0, 0, 0), Vector.UP),
        #            CheckerboardSurface())
        #render(s,canvas1)
    return None

"""
generate_bench_raytrace = generate_function("bench_raytrace", bench_raytrace_params, bench_raytrace_anns, bench_raytrace_body)

rest = """

    #dt = perf.perf_counter() - t0

    #if filename:
    #    canvas.write_ppm(filename)
    #return dt
def main():
    t0 = time.time()
    bench_raytrace(100, DEFAULT_WIDTH, DEFAULT_HEIGHT, "raytrace.ppm")
    t1 = time.time()
    print(t1-t0)

main()

"""
vector_start = 0
vector_end = len(vector_params)
magnitude_start = vector_end 
magnitude_end = magnitude_start + len(magnitude_params)
dot_start = magnitude_end 
dot_end = dot_start + len(dot_params)
add_start = dot_end 
add_end = add_start + len(add_params)
sub_start = add_end 
sub_end = sub_start + len(sub_params)
scale_start = sub_end 
scale_end = scale_start + len(scale_params)
cross_start = scale_end 
cross_end = cross_start + len(cross_params)
normalized_start = cross_end 
normalized_end = normalized_start + len(normalized_params)
negated_start = normalized_end 
negated_end = negated_start + len(negated_params)
eq_start = negated_end 
eq_end = eq_start + len(eq_params)
reflectThrough_start = eq_end 
reflectThrough_end = reflectThrough_start + len(reflectThrough_params)
sphere_start = reflectThrough_end 
sphere_end = sphere_start + len(sphere_params)
ray_start = sphere_end 
ray_end = ray_start + len(ray_params)
pointAtTime_start = ray_end 
pointAtTime_end = pointAtTime_start + len(pointAtTime_params)
intersectionTime_start = pointAtTime_end 
intersectionTime_end = intersectionTime_start + len(intersectionTime_params)
normalAt_start = intersectionTime_end
normalAt_end = normalAt_start + len(normalAt_params)
halfspace_start = normalAt_end 
halfspace_end = halfspace_start + len(halfspace_params)
intersectionTime1_start = halfspace_end 
intersectionTime1_end = intersectionTime1_start + len(intersectionTime1_params)
normalAt1_start = intersectionTime1_end 
normalAt1_end = normalAt1_start + len(normalAt1_params)
canvas_start = normalAt1_end 
canvas_end = canvas_start + len(canvas_params)
plot_start = canvas_end 
plot_end = plot_start + len(plot_params)
firstIntersection_start = plot_end 
firstIntersection_end = firstIntersection_start + len(firstIntersection_params)
scene_start = firstIntersection_end 
scene_end = scene_start + len(scene_params)
moveTo_start = scene_end 
moveTo_end = moveTo_start + len(moveTo_params)
lookAt_start = moveTo_end 
lookAt_end = lookAt_start + len(lookAt_params)
addObject_start = lookAt_end 
addObject_end = addObject_start + len(addObject_params)
addLight_start = addObject_end 
addLight_end = addLight_start + len(addLight_params)
addColours_start = addLight_end 
addColours_end = addColours_start + len(addColours_params)
baseColourAt_start = addColours_end 
baseColourAt_end = baseColourAt_start + len(baseColourAt_params)
#colourAt = colourAt_headers[random.randrange(0,len(colourAt_headers))] + colourAt_body
#rayColour = rayColour_headers[random.randrange(0,len(rayColour_headers))] + rayColour_body
render_start = baseColourAt_end 
render_end = render_start + len(render_params)
lightIsVisible_start = render_end 
lightIsVisible_end = lightIsVisible_start + len(lightIsVisible_params)
visibleLights_start = lightIsVisible_end 
visibleLights_end = visibleLights_start + len(visibleLights_params)
simpleSurface_start = visibleLights_end 
simpleSurface_end = simpleSurface_start + len(simpleSurface_params)
bench_raytrace_start = simpleSurface_end 
bench_raytrace_end = bench_raytrace_start + len(bench_raytrace_params)
def copy_range(bit_vector, start, end):
    row = [0] * bench_raytrace_end
    for i in range(start, end):
        row[i] = bit_vector[i]
    return row

def bit_matrix_to_bit_vector(bit_matrix):
    vector_from_matrix = [0] * len(bit_matrix[0])
    for row in bit_matrix:
        for index, el in enumerate(row):
            vector_from_matrix[index] |=  el
    return vector_from_matrix

def bit_vector_to_bit_matrix(bit_vector):
    vector = copy_range(bit_vector, vector_start, vector_end)
    dot = copy_range(bit_vector, dot_start, dot_end)
    magnitude = copy_range(bit_vector, magnitude_start, magnitude_end)
    add = copy_range(bit_vector, add_start, add_end)
    sub = copy_range(bit_vector, sub_start, sub_end)
    scale = copy_range(bit_vector, scale_start, scale_end)
    cross = copy_range(bit_vector, cross_start, cross_end)
    normalized = copy_range(bit_vector, normalized_start, normalized_end)
    negated = copy_range(bit_vector, negated_start, negated_end)
    eq = copy_range(bit_vector, eq_start, eq_end)
    reflectThrough = copy_range(bit_vector, reflectThrough_start, reflectThrough_end)
    sphere = copy_range(bit_vector, sphere_start, sphere_end)
    ray = copy_range(bit_vector, ray_start, ray_end)
    pointAtTime = copy_range(bit_vector, pointAtTime_start, pointAtTime_end)
    intersectionTime = copy_range(bit_vector, intersectionTime_start, intersectionTime_end)
    normalAt = copy_range(bit_vector, normalAt_start, normalAt_end)
    halfspace = copy_range(bit_vector, halfspace_start, halfspace_end)
    intersectionTime1 = copy_range(bit_vector, intersectionTime1_start, intersectionTime1_end)
    normalAt1 = copy_range(bit_vector, normalAt1_start, normalAt1_end)
    canvas = copy_range(bit_vector, canvas_start, canvas_end)
    plot = copy_range(bit_vector, plot_start, plot_end)
    firstIntersection = copy_range(bit_vector, firstIntersection_start, firstIntersection_end)
    scene = copy_range(bit_vector, scene_start, scene_end)
    moveTo = copy_range(bit_vector, moveTo_start, moveTo_end)
    lookAt = copy_range(bit_vector, lookAt_start, lookAt_end)
    addObject = copy_range(bit_vector, addObject_start, addObject_end)
    addLight = copy_range(bit_vector, addLight_start, addLight_end)
    addColours = copy_range(bit_vector, addColours_start, addColours_end)
    baseColourAt = copy_range(bit_vector, baseColourAt_start, baseColourAt_end)
    #colourAt = colourAt_headers[random.randrange(0,len(colourAt_headers))] + colourAt_body
    #rayColour = rayColour_headers[random.randrange(0,len(rayColour_headers))] + rayColour_body
    render = copy_range(bit_vector, render_start, render_end)
    lightIsVisible = copy_range(bit_vector, lightIsVisible_start, lightIsVisible_end)
    visibleLights = copy_range(bit_vector, visibleLights_start, visibleLights_end)
    simpleSurface = copy_range(bit_vector, simpleSurface_start, simpleSurface_end)
    bench_raytrace = copy_range(bit_vector, bench_raytrace_start, bench_raytrace_end)
    main_row = [0] * bench_raytrace_end
    return [vector , dot , magnitude , add , sub , scale , cross , normalized , negated , eq , reflectThrough , sphere , ray , pointAtTime , intersectionTime , normalAt , halfspace , intersectionTime1 , normalAt1 , canvas , plot , firstIntersection , scene , moveTo , lookAt , addObject , addLight , addColours , baseColourAt , render , lightIsVisible , visibleLights , simpleSurface, bench_raytrace, main_row]
    
def generate_program(bit_vector):
    with open("raytrace_test.py","w+") as f:
        vector = generate_vector(bit_vector[vector_start:vector_end])
        dot = generate_dot(bit_vector[dot_start:dot_end])
        magnitude = generate_magnitude(bit_vector[magnitude_start:magnitude_end])
        add = generate_add(bit_vector[add_start:add_end])
        sub = generate_sub(bit_vector[sub_start:sub_end])
        scale = generate_scale(bit_vector[scale_start:scale_end])
        cross = generate_cross(bit_vector[cross_start:cross_end])
        normalized = generate_normalized(bit_vector[normalized_start:normalized_end])
        negated = generate_negated(bit_vector[negated_start:negated_end])
        eq = generate_eq(bit_vector[eq_start:eq_end])
        reflectThrough = generate_reflectThrough(bit_vector[reflectThrough_start:reflectThrough_end])
        sphere = generate_sphere(bit_vector[sphere_start:sphere_end])
        ray = generate_ray(bit_vector[ray_start:ray_end])
        pointAtTime = generate_pointAtTime(bit_vector[pointAtTime_start:pointAtTime_end])
        intersectionTime = generate_intersectionTime(bit_vector[intersectionTime_start:intersectionTime_end])
        normalAt = generate_normalAt(bit_vector[normalAt_start:normalAt_end])
        halfspace = generate_halfspace(bit_vector[halfspace_start:halfspace_end])
        intersectionTime1 = generate_intersectionTime1(bit_vector[intersectionTime1_start:intersectionTime1_end])
        normalAt1 = generate_normalAt1(bit_vector[normalAt1_start:normalAt1_end])
        canvas = generate_canvas(bit_vector[canvas_start:canvas_end])
        plot = generate_plot(bit_vector[plot_start:plot_end])
        firstIntersection = generate_firstIntersection(bit_vector[firstIntersection_start:firstIntersection_end])
        scene = generate_scene(bit_vector[scene_start:scene_end])
        moveTo = generate_moveTo(bit_vector[moveTo_start:moveTo_end])
        lookAt = generate_lookAt(bit_vector[lookAt_start:lookAt_end])
        addObject = generate_addObject(bit_vector[addObject_start:addObject_end])
        addLight = generate_addLight(bit_vector[addLight_start:addLight_end])
        addColours = generate_addColours(bit_vector[addColours_start:addColours_end])
        baseColourAt = generate_baseColourAt(bit_vector[baseColourAt_start:baseColourAt_end])
        #colourAt = colourAt_headers[random.randrange(0,len(colourAt_headers))] + colourAt_body
        #rayColour = rayColour_headers[random.randrange(0,len(rayColour_headers))] + rayColour_body
        render = generate_render(bit_vector[render_start:render_end])
        lightIsVisible = generate_lightIsVisible(bit_vector[lightIsVisible_start:lightIsVisible_end])
        visibleLights = generate_visibleLights(bit_vector[visibleLights_start:visibleLights_end])
        simpleSurface = generate_simpleSurface(bit_vector[simpleSurface_start:simpleSurface_end])
        bench_raytrace = generate_bench_raytrace([0, 0, 0, 0])
        #main = main_headers[random.randrange(0,len(main_headers))] + main_body

        out = header + vector + dot + magnitude + add + sub + scale + cross + normalized + negated + eq + reflectThrough + sphere + ray + pointAtTime + intersectionTime + normalAt + halfspace + intersectionTime1 + normalAt1 + canvas + plot + firstIntersection + scene + moveTo + lookAt + addObject + addLight + addColours + baseColourAt + render + lightIsVisible + visibleLights + simpleSurface + bench_raytrace + rest
        f.write(out)
        #f.close()
    cmd = ['retic', '--guarded', 'raytrace_test.py']
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    time_string = ""
    for line in p.stdout:
        time_string = line
    stripped_time_string = time_string.strip()
    time = float(stripped_time_string)
    #print(time)
    p.wait()
    return time

def bit_string(fname, start, end):
    return "{0} bits are {1}-{2}".format(fname, start, end)

def main():
    print(bit_string("vector", vector_start, vector_end))
    print(bit_string("dot", dot_start, dot_end))
    print(bit_string("magnitude", magnitude_start, magnitude_end))
    print(bit_string("add", add_start, add_end))    
    print(bit_string("sub", sub_start, sub_end))
    print(bit_string("scale", scale_start, scale_end))
    print(bit_string("cross", cross_start, cross_end))
    print(bit_string("normalized", normalized_start, normalized_end))
    print(bit_string("negated", negated_start, negated_end))
    print(bit_string("eq", eq_start, eq_end))
    print(bit_string("reflectThrough", reflectThrough_start, reflectThrough_end))
    print(bit_string("sphere", sphere_start, sphere_end))
    print(bit_string("ray", ray_start, ray_end))
    print(bit_string("pointAtTime", pointAtTime_start, pointAtTime_end))
    print(bit_string("intersectionTime", intersectionTime_start, intersectionTime_end))
    print(bit_string("normalAt", normalAt_start, normalAt_end))
    print(bit_string("halfspace", halfspace_start, halfspace_end))
    print(bit_string("intersectionTime1", intersectionTime1_start, intersectionTime1_end))
    print(bit_string("normalAt1", normalAt1_start, normalAt1_end))
    print(bit_string("canvas", canvas_start, canvas_end))
    print(bit_string("plot", plot_start, plot_end))
    print(bit_string("firstIntersection", firstIntersection_start, firstIntersection_end))
    print(bit_string("scene", scene_start, scene_end))
    print(bit_string("moveTo", moveTo_start, moveTo_end))
    print(bit_string("lookAt", lookAt_start, lookAt_end))
    print(bit_string("addObject", addObject_start, addObject_end))
    print(bit_string("addLight", addLight_start, addLight_end))
    print(bit_string("addColours", addColours_start, addColours_end))
    print(bit_string("baseColourAt", baseColourAt_start, baseColourAt_end))
    print(bit_string("render", render_start, render_end))
    print(bit_string("lightIsVisible", lightIsVisible_start, lightIsVisible_end))
    print(bit_string("visibleLights", visibleLights_start, visibleLights_end))
    print(bit_string("simpleSurface", simpleSurface_start, simpleSurface_end))
    print(bit_string("bench_raytrace", bench_raytrace_start, bench_raytrace_end))
    
if (__name__ == "__main__"):
    #main()
    #generate_program([0,0,0,1,0,1,1,1,0,0,0,0,1,0,0,1,0,0,1,0,0,0,0,1,0,0,1,1,1,1,0,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,0,1,0,1,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0])
    test_vector = [0,0,0,1,0,1,1,1,0,0,0,0,1,0,0,1,0,0,1,0,0,0,0,1,0,0,1,1,1,1,0,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,0,1,0,1,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0]
    matrix = bit_vector_to_bit_matrix(test_vector)
    output_vector = bit_matrix_to_bit_vector(matrix)
    print(test_vector)
    print(output_vector)
    print(test_vector == output_vector)
    print(len(matrix))
