import random
import pdb
import subprocess
callgraph = [[0, 0, 2, 2, 2], [1, 2, 3, 4, 5]]
def generate_header(function_name, param_names, param_anns, sub_vector):
    """
    String List[String] List[String] Slice[Bit] -> String
    This function accepts a function name, its corresponding parameter names,
    the possible annotations for these parameters, and a slice of the global
    parameter bit vector that corresponds to the parameter indexes.    
    It produces a header with proper type annotations added
    """
    params = [(param_names[index] + param_anns[index] if value else param_names[index]) for index, value in enumerate(sub_vector)]
    param_string = ",".join(params)
    header = "def {0}({1})->Dyn:".format(function_name, param_string)
    return header

def generate_function(function_name, param_names, param_anns, function_body):
    """
    String List[String] List[String] -> (Slice[bit] -> String)
    This function generates a function body with annotations specified by sub_vector
    """
    return (lambda sub_vector: generate_header(function_name, param_names, param_anns, sub_vector) + function_body)



programHead = """
from six.moves import xrange
import pdb
from itertools import islice
import time
__contact__ = "collinwinter@google.com (Collin Winter)"
DEFAULT_ITERATIONS = 10000
DEFAULT_REFERENCE = 'sun'
    
"""

combinations_headers = ["def combinations(l)->List(Dyn):",
                        "def combinations(l:List(Dyn))->List(Dyn):"]
combinations_params = ["l"]
combinations_anns = [":List(Dyn)"]
combinations_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


PI = 3.14159265358979323
SOLAR_MASS = 4 * PI * PI
DAYS_PER_YEAR = 365.24
#pdb.set_trace()
BODIES = {
'sun': ([0.0, 0.0, 0.0], [0.0, 0.0, 0.0], SOLAR_MASS),

'jupiter': ([4.84143144246472090e+00,
             -1.16032004402742839e+00,
             -1.03622044471123109e-01],
            [1.66007664274403694e-03 * DAYS_PER_YEAR,
              7.69901118419740425e-03 * DAYS_PER_YEAR,
             -6.90460016972063023e-05 * DAYS_PER_YEAR],
            9.54791938424326609e-04 * SOLAR_MASS),

'saturn': ([8.34336671824457987e+00,
           4.12479856412430479e+00,
            -4.03523417114321381e-01],
          [(-2.76742510726862411e-03 * DAYS_PER_YEAR),
            (4.99852801234917238e-03 * DAYS_PER_YEAR),
            (2.30417297573763929e-05 * DAYS_PER_YEAR)],
          (2.85885980666130812e-04 * SOLAR_MASS)),

'uranus': ([1.28943695621391310e+01,
            -1.51111514016986312e+01,
            -2.23307578892655734e-01],
           [2.96460137564761618e-03 * DAYS_PER_YEAR,
            2.37847173959480950e-03 * DAYS_PER_YEAR,
            -2.96589568540237556e-05 * DAYS_PER_YEAR],
           4.36624404335156298e-05 * SOLAR_MASS),

'neptune': ([1.53796971148509165e+01,
             -2.59193146099879641e+01,
             1.79258772950371181e-01],
            [2.68067772490389322e-03 * DAYS_PER_YEAR,
             1.62824170038242295e-03 * DAYS_PER_YEAR,
             -9.51592254519715870e-05 * DAYS_PER_YEAR],
             5.15138902046611451e-05 * SOLAR_MASS)}


SYSTEM = list(BODIES.values())
PAIRS = combinations(SYSTEM)
    
"""
generate_combinations = generate_function("combinations", combinations_params, combinations_anns, combinations_body)

advance_headers = ["def advance(dt, n, bodies, pairs):",
                       "def advance(dt:float, n, bodies, pairs):",
                       "def advance(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                       "def advance(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                       "def advance(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                       "def advance(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                       "def advance(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                       "def advance(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

    #below are the possible annotations for advance's parameters
    
    #below we stick the possible annotations into a gloval list of possible parameter annotations
advance_params = ["dt", "n", "bodies", "pairs"]
advance_anns = [":float", ":int", ":List(Tuple(Dyn,Dyn,Dyn))", ":List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))"]
advance_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""
    #generates the defintion for advance
generate_advance = generate_function("advance", advance_params, advance_anns, advance_body)

report_energy_headers = ["def report_energy(bodies, pairs, e):",
                             "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                             "def report_energy(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                             "def report_energy(bodies, pairs, e:float)->float:",
                             "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                             "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                             "def report_energy(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                             "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:"
    ]

report_energy_params = "bodies, pairs, e".split(',')
report_energy_anns = [":List(Tuple(Dyn,Dyn,Dyn))", ":List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))", ":float"]
report_energy_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""
    #generates the defintion for report_energy
generate_report_energy = generate_function("report_energy", report_energy_params, report_energy_anns, report_energy_body)

offset_momentum_headers = ["def offset_momentum(ref, bodies, px, py, pz):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                               "def offset_momentum(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px, py, pz):",
                               "def offset_momentum(ref, bodies, px:float, py, pz):",
                               "def offset_momentum(ref, bodies, px, py:float, pz:float):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px, py, pz):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py, pz):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py:float, pz):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz:float):",
                               "def offset_momentum(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px, py, pz):",
                               "def offset_momentum(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py, pz):",
                               "def offset_momentum(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px, py:float, pz):",
                               "def offset_momentum(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px, py, pz:float):",
                               "def offset_momentum(ref, bodies, px:float, py:float, pz):",
                               "def offset_momentum(ref, bodies, px:float, py, pz:float):",
                               "def offset_momentum(ref, bodies, px, py:float, pz:float):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py, pz):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px, py:float, pz):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px, py, pz:float):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py, pz:float):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py:float, pz:float):",
                               "def offset_momentum(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz):",
                               "def offset_momentum(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py, pz:float):",
                               "def offset_momentum(ref, bodies, px:float, py:float, pz:float):",                           
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py, pz:float):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px, py:float, pz:float):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",              
                               "def offset_momentum(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                               "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"
    ]
offset_momentum_params = "ref, bodies, px, py, pz".split(',')
offset_momentum_anns = [":Tuple(Dyn,Dyn,Dyn)", ":List(Tuple(Dyn,Dyn,Dyn))", ":float", ":float", ":float"]
offset_momentum_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""
generate_offset_momentum = generate_function("offset_momentum", offset_momentum_params, offset_momentum_anns, offset_momentum_body)

bench_nbody_params = "loops, reference, iterations".split(",")
bench_nbody_anns = [":int", ":string", ":int"]
bench_nbody_body = """
    # Set up global state
    offset_momentum(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)
    range_it = xrange(loops)
    for _ in range_it:
        report_energy(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy(SYSTEM, PAIRS, 0.0)
    return None
"""
generate_bench_nbody = generate_function("bench_nbody", bench_nbody_params, bench_nbody_anns, bench_nbody_body)

rest = """
def main():
    bench_nbody(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

t0 = time.time()
main()
t1 = time.time()
print(t1-t0)
"""

indexList = []

    # Here we track where parameter lists start and
    # end in the global bit vector.
combinations_start = 0
combinations_end = len(combinations_params)
advance_start = combinations_end 
advance_end = advance_start + len(advance_params)
report_energy_start = advance_end 
report_energy_end = report_energy_start + len(report_energy_params)
offset_momentum_start = report_energy_end 
offset_momentum_end = offset_momentum_start + len(offset_momentum_params)
bench_nbody_start = offset_momentum_end
bench_nbody_end = bench_nbody_start + len(bench_nbody_params)

num_params = len(combinations_params) + len(advance_params) + len(report_energy_params) + len(offset_momentum_params) + len(bench_nbody_params)

def generate_program(bit_vector):
    combinations = generate_combinations(bit_vector[combinations_start:combinations_end])
    advance = generate_advance(bit_vector[advance_start:advance_end])
    report_energy = generate_report_energy(bit_vector[report_energy_start:report_energy_end])
    offset_momentum = generate_offset_momentum(bit_vector[offset_momentum_start:offset_momentum_end])
    bench_nbody = generate_bench_nbody(bit_vector[bench_nbody_start:bench_nbody_end])

    program = programHead + combinations + advance + report_energy + offset_momentum + bench_nbody + rest
    programName = "nbody" + "_test" + ".py"
    with open(programName, 'w+') as f:
        f.write(program)
    cmd = ['retic', '--guarded', 'nbody_test.py']
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    time_string = ""
    for line in p.stdout:
        time_string = line
    stripped_time_string = time_string.strip()
    time = float(stripped_time_string)
    #print(time)
    p.wait()
    return time

def bit_vector_to_bit_matrix(bit_vector):
    combinations_actual_bits = bit_vector[combinations_start:combinations_end]
    combinations_end_bits = [0] * (bench_nbody_end - combinations_end)
    combinations_row = combinations_actual_bits + combinations_end_bits
    advance_start_bits = [0] * advance_start
    advance_actual_bits = bit_vector[advance_start:advance_end]
    advance_end_bits = [0] * (bench_nbody_end - advance_end)
    advance_row = advance_start_bits + advance_actual_bits + advance_end_bits
    report_energy_start_bits =  [0] * report_energy_start
    report_energy_actual_bits = bit_vector[report_energy_start:report_energy_end]
    report_energy_end_bits = [0] * (bench_nbody_end - report_energy_end)
    report_energy_row = report_energy_start_bits + report_energy_actual_bits + report_energy_end_bits
    offset_momentum_start_bits = [0] * offset_momentum_start
    offset_momentum_actual_bits = bit_vector[offset_momentum_start:bench_nbody_end]
    offset_momentum_end_bits = [0] * (bench_nbody_end - offset_momentum_end)
    offset_momentum_row = offset_momentum_start_bits + offset_momentum_actual_bits
    bench_nbody_start_bits = [0] * bench_nbody_start
    bench_nbody_actual_bits = bit_vector[bench_nbody_start:bench_nbody_end]
    bench_nbody_row = bench_nbody_start_bits + bench_nbody_actual_bits
    main_row = [0] * bench_nbody_end
    
    return [combinations_row, advance_row, report_energy_row, offset_momentum_row, bench_nbody_row, main_row]

def main():
    bit_vector = [0] * num_params
    #pdb.set_trace()
    i = 0
    while i <= num_params:
        j = 0
        while j < i:
            param_to_annotate = random.randrange(num_params)
            if not bit_vector[param_to_annotate]:
                bit_vector[param_to_annotate] = 1
                j += 1

        #if not (combinations_index, advance_index, report_energy_index,  offset_momentum_index) in indexList:
        #    indexList.append((combinations_index, advance_index, report_energy_index,  offset_momentum_index))
        combinations = generate_combinations(bit_vector[combinations_start:combinations_end])
        advance = generate_advance(bit_vector[advance_start:advance_end])
        report_energy = generate_report_energy(bit_vector[report_energy_start:report_energy_end])
        offset_momentum = generate_offset_momentum(bit_vector[offset_momentum_start:offset_momentum_end])

        program = programHead + combinations + advance + report_energy + offset_momentum + rest
        programName = "nbody" + str(i) + ".py"
        with open(programName, 'w+') as f:
            f.write(program)
        bit_vector = [0] * num_params
        i += 1
        #print(program)

if (__name__ == "__main__"):
    #generate_program([0,1,0,0,1,0,0,0,0,1,0,1,0,0,0,0])
    sample_matrix = bit_vector_to_bit_matrix([0,1,0,0,1,0,0,0,0,1,0,1,0,1,1,1])
    #print(len(sample_matrix))
    #print()
