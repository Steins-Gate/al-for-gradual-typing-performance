"""
This file contains definitions for a simple raytracer.
Copyright Callum and Tony Garnock-Jones, 2008.
This file may be freely redistributed under the MIT license,
http://www.opensource.org/licenses/mit-license.php
From http://www.lshift.net/blog/2008/10/29/toy-raytracer-in-python
"""

import array
import math
import time
import perf
import pdb
from six.moves import xrange


DEFAULT_WIDTH = 100
DEFAULT_HEIGHT = 100
EPSILON = 0.00001
ZERO = (0, 0, 0)#vector(0, 0, 0)
RIGHT = (1, 0, 0)#vector(1, 0, 0)
UP = (0, 1, 0)#vector(0, 1, 0)
OUT = (0, 0, 1)#vector(0, 0, 1)



def vector(initx, inity, initz):
    return (initx, inity, initz)

#def vectorstr(vec):
#    (x,y,z) = vec
#    return '(%s,%s,%s)' % (x, y, z)

#def __repr__(vec):
#    (x,y,z) = vec
#    return 'Vector(%s,%s,%s)' % (x, y, z)

def dot(vec, other):
    #other.mustBeVector()
    (x,y,z) = vec
    (x1, y1, z1) = other
    return (x * x1 * 1.0) + (y * y1 * 1.0) + (z * z1 * 1.0)
    
def magnitude(vec):        
    return (math.sqrt(dot(vec, vec)) + 0.0)

def add(vec, other):
    (x,y,z) = vec
    (x1,y1,z1) = other
    return (x+x1, y+y1, z+z1)
    #if other.isPoint():
        #return Point(self.x + other.x, self.y + other.y, self.z + other.z)
    #else:
        #return Vector(self.x + other.x, self.y + other.y, self.z + other.z)

def sub(vec, other):
    #other.mustBeVector()
    (x,y,z) = vec
    (x1,y1,z1) = other
    return vector(x - x1, y - y1, z - z1)

def scale(vec, factor):
    x,y,z = vec
    return (factor * x, factor * y, factor * z)

    

def cross(vec, other):
    #other.mustBeVector()
    (x,y,z) = vec
    (x1,y1,z1) = other
    return (y * z1 - z * y1,
                      z * x1 - x * z1,
                      x * y1 - y * x1)

def normalized(vec):
    return scale(vec, 1.0 / magnitude(vec))

def negated(vec):
    return scale(vec, -1)

def eq(vec, other):
    x,y,z = vec
    x1,y1,z1 = other
    return (x == x1) and (y == y1) and (z == z1)

#def isVector(self):
#    return True

#def isPoint(self):
#    return False

#def mustBeVector(self):
#    return self

#def mustBePoint(self):
#    raise 'Vectors are not points!'

def reflectThrough(vec, normal):
    d = scale(normal, dot(vec, normal))
    return sub(vec, scale(d,2))




#assert Vector.RIGHT.reflectThrough(Vector.UP) == Vector.RIGHT
#assert Vector(-1, -1, 0).reflectThrough(Vector.UP) == Vector(-1, 1, 0)


#class Point(object):

#    def __init__(self, initx, inity, initz):
#        self.x = initx
#        self.y = inity
#        self.z = initz

#    def __str__(self):
#        return '(%s,%s,%s)' % (self.x, self.y, self.z)

#    def __repr__(self):
#        return 'Point(%s,%s,%s)' % (self.x, self.y, self.z)

#    def __add__(self, other):
#        other.mustBeVector()
#        return Point(self.x + other.x, self.y + other.y, self.z + other.z)

#    def __sub__(self, other):
#        if other.isPoint():
#            return Vector(self.x - other.x, self.y - other.y, self.z - other.z)
#        else:
#            return Point(self.x - other.x, self.y - other.y, self.z - other.z)

#    def isVector(self):
#        return False

#    def isPoint(self):
#        return True

#    def mustBeVector(self):
#        raise 'Points are not vectors!'

#    def mustBePoint(self):
#        return self


#class Sphere(object):

def sphere(centre, radius):
    #centre.mustBePoint()
    #self.centre = centre
    #self.radius = radius
    return (centre, radius)

#def __repr__(self):
#    return 'Sphere(%s,%s)' % (repr(self.centre), self.radius)

#class Ray(object):

def ray(point, vect):
    #self.point = point
    #self.vector = vector.normalized()
    return (point, normalized(vect))

#def __repr__(self):
#    return 'Ray(%s,%s)' % (repr(self.point), repr(self.vector))

def pointAtTime(ray, t):
    point, vector = ray
    return add(point, scale(vector, t))

def intersectionTime(s, ray):
    (centre, radius) = s
    (point, vector) = ray
    cp = sub(centre, point)
    v = dot(cp, vector)
    discriminant = (radius * radius) - (dot(cp, cp) - v * v)
    if discriminant < 0:
        return None
    else:
        return v - math.sqrt(discriminant)

def normalAt(s, p):
    (centre, radius) = s
    return normalized((sub(p,  centre)))


#class Halfspace(object):

def halfspace(point, normal):
    #self.point = point
    #self.normal = normal.normalized()
    return (point, normalized(normal))

#def __repr__(self):
#    return 'Halfspace(%s,%s)' % (repr(self.point), repr(self.normal))

def intersectionTime1(hs, ray):
    point, vector = ray
    (point, normal) = hs
    v = dot(vector, normal)
    if v:
        return 1 / -v
    else:
        return v

def normalAt1(hs, p):
    (p1, normal) = hs
    return normal





#Point.ZERO = Point(0, 0, 0)


#class Canvas(object):

def canvas(width, height):        
    byts = array.array('B', [0] * (width * height * 3))
    for i in xrange(width * height):
        #0
        byts[i * 3 + 2] = 255
    #self.width = width
    #self.height = height
    return (byts, width, height)

def plot(canv, x, y, r, g, b):
    (byts, width, height) = canv
    i = ((height - y - 1) * width + x) * 3
    byts[i] = max(0, min(255, int(r * 255)))
    byts[i + 1] = max(0, min(255, int(g * 255)))
    byts[i + 2] = max(0, min(255, int(b * 255)))
    return None

#def write_ppm(self, filename):
#    header = 'P6 %d %d 255\n' % (self.width, self.height)
#    with open(filename, "wb") as fp:
#        fp.write(header.encode('ascii'))
#        fp.write(self.bytes.tostring())


def firstIntersection(intersections):
    result = None
    for i in intersections:
        candidateT = i[1]
        if candidateT is not None and candidateT > -EPSILON:
            if result is None or candidateT < result[1]:
                result = i
    return result


#class Scene(object):

def scene():
    objects = []
    lightPoints = []
    position = vector(0.0, 1.8, 10.0)
    lookingAt = (0.0,0.0,0.0)#Point.ZERO
    fieldOfView = 45
    recursionDepth = 0
    return (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

def moveTo(sc, p):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    position = p
    return (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

def lookAt(sc, p):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    lookingAt = p
    return (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

def addObject(sc, object, surface):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    objs = [] + objects
    objs.append((object, surface))
    return (objs, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

def addLight(sc, p):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    lps = [] + lightPoints
    lps.append(p)
    return (objects, lps, position, lookingAt, fieldOfView, recursionDepth)

def addColours(a, scale, b):
    return (a[0] + scale * b[0],
            a[1] + scale * b[1],
            a[2] + scale * b[2])

def baseColourAt(ss, p):
    (baseColour, specularCoefficient, lambertCoefficient, ambientCoefficient) = ss
    return baseColour

"""def colourAt(ss, scene1, ray1, p1, normal1):
    (baseColour, specularCoefficient, lambertCoefficient, ambientCoefficient)
    b = baseColourAt(ss, p)
    (p,v) = ray1

    c = (0, 0, 0)
    if specularCoefficient > 0:
        reflectedRay = ray(p1, reflectThrough(v, vnormal))
        reflectedColour = rayColour(scene1,reflectedRay)
        c = addColours(c, specularCoefficient, reflectedColour)

    if lambertCoefficient > 0:
        lambertAmount = 0
        for lightPoint in visibleLights(scene1, p1):
            contribution = dot(normalized((sub(lightPoint, p1))), normal)
            if contribution > 0:
                lambertAmount = lambertAmount + contribution
        lambertAmount = min(1, lambertAmount)
        c = addColours(c, lambertCoefficient * lambertAmount, b)

    if ambientCoefficient > 0:
        c = addColours(c, ambientCoefficient, b)

    return c



def rayColour(sc, ry):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    if recursionDepth > 3:
        return (0.0, 0.0, 0.0)
    try:
        recursionDepth = recursionDepth + 1
        intersections = [(o, intersectionTime(o,ry), s)
                             for (o, s) in objects]
        i = firstIntersection(intersections)
        if i is None:
            return (0.0, 0.0, 0.0)  # the background colour
        else:
            (o, t, s) = i
            p = pointAtTime(ry, t)
            return colourAt(s, sc, ry, p, o.normalAt(p))
    finally:
        recursionDepth = recursionDepth - 1
    return (0.0,0.0,0.0)"""


def render(sc, canvas1):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    fovRadians = math.pi * (fieldOfView / 2.0) / 180.0
    halfWidth = math.tan(fovRadians)
    halfHeight = 0.75 * halfWidth
    width = halfWidth * 2
    height = halfHeight * 2
    byts, w, h = canvas1
    pixelWidth = width / (w - 1)
    pixelHeight = height / (h - 1)

    eye = ray(position, sub(lookingAt, position))
    p, v = eye    
    vpRight = normalized(cross(v, UP))
    vpUp = normalized(cross(vpRight, v))

    for y in xrange(int(height)):
        for x in xrange(int(width)):
            xcomp = scale(vpRight, x * pixelWidth - halfWidth)
            ycomp = scale(vpUp, y * pixelHeight - halfHeight)
            r = ray(p, add(add(v, xcomp), ycomp))
            #colour = rayColour(sc, r)
            #plot(canvas1, x, y, *colour)

    return None

    
#def lightIsVisible(sc, l, p):
#    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
#    for (o, s) in objects:
#        t = intersectionTime(o, ray(p, sub(l, p)))
#        if t is not None and t > EPSILON:
#            return False
#    return True

#def visibleLights(sc, p):
#    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
#    result = []
#    for l in lightPoints:
#        if lightIsVisible(sc, l, p):
#            result.append(l)
#    return result




#class SimpleSurface(object):

def simpleSurface(baseColour):
    #baseColour = kwargs.get('baseColour', (1, 1, 1))
    specularCoefficient = 0.2
    lambertCoefficient =  0.6
    ambientCoefficient = 1.0 - specularCoefficient - lambertCoefficient
    return (baseColour, specularCoefficient, lambertCoefficient, ambientCoefficient)


#class CheckerboardSurface(SimpleSurface):

#    def __init__(self, **kwargs):
#        SimpleSurface.__init__(self, **kwargs)
#        self.otherColour = kwargs.get('otherColour', (0, 0, 0))
#        self.checkSize = kwargs.get('checkSize', 1)

#    def baseColourAt(self, p):
#        v = p - Point.ZERO
#        v.scale(1.0 / self.checkSize)
#        if (int(abs(v.x) + 0.5) +
#            int(abs(v.y) + 0.5) +
#            int(abs(v.z) + 0.5)) \
#           % 2:
#            return self.otherColour
#        else:
#            return self.baseColour


def bench_raytrace(loops, width, height, filename):
    range_it = xrange(loops)
    #t0 = perf.perf_counter()

    for i in range_it:
        #canvas1 = canvas(width, height)
        s = scene()
        addLight(s, vector(30, 30, 10))
        #addLight(s, vector(-10, 100, 30))
        lookAt(s, vector(0, 3, 0))
        addObject(s, sphere(vector(1, 3, -10), 2),
                    simpleSurface((1, 1, 0)))
        for y in xrange(6):
            #addObject(s,sphere(vector(-3 - y * 0.4, 2.3, -5), 0.4),
            #            simpleSurface((y / 6.0, 1 - y / 6.0, 0.5)))
            scale(normalized(vector(10,23,19)), y * 11)
        #s.addObject(Halfspace(Point(0, 0, 0), Vector.UP),
        #            CheckerboardSurface())
        #render(s,canvas1)
    return None

    #dt = perf.perf_counter() - t0

    #if filename:
    #    canvas.write_ppm(filename)
    #return dt
def main():

    bench_raytrace(100, DEFAULT_WIDTH, DEFAULT_HEIGHT, "raytrace.ppm")


t0 = time.time()
main()
t1 = time.time()
print(t1-t0)
