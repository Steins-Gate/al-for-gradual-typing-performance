from typing import List
import functools
import subprocess
import random
callgraph = [[0, 1, 1, 1, 2, 2, 2, 3, 3, 4],[1, 2, 3, 4, 3, 4, 5, 4, 5, 5]]
def generate_program(bit_vector: List[int]) -> float:
    bit_vector_to_numstring = functools.reduce((lambda x, y: x + y), map(str, bit_vector))
    cmd = ['racket', 'setup-benchmark.rkt', 'suffixtree', bit_vector_to_numstring]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    p.wait()
    cmd = ['racket', 'run.rkt', 'suffixtree']
    p1 = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    time_string = ""
    for line in p1.stdout:
        time_string = line
    stripped_time_string = time_string.strip()
    time = float(stripped_time_string)
    #convert time to seconds
    time = time / 1000
    print(time)
    p1.wait()
    return time

def bit_vector_to_bit_matrix(bit_vector: List[int]) -> List[List[int]]:
    # initializer an nxn bit_matrix for a vector in n dimensions
    bit_matrix = [[0 for _ in range(len(bit_vector))] for _ in range(len(bit_vector))]
    for i in range(len(bit_vector)):
        # copy the diagonal
        bit_matrix[i][i] = bit_vector[i]
    return bit_matrix

def bit_matrix_to_bit_vector(bit_matrix: List[List[int]]) -> List[int]:
    return [bit_matrix[i][i] for i in range(len(bit_matrix))]

if __name__ == "__main__":
    bit_vector = [random.randrange(0, 2) for _ in range(6)]
    bit_matrix = bit_vector_to_bit_matrix(bit_vector)
    print(bit_vector == bit_matrix_to_bit_vector(bit_matrix))
    generate_program(bit_vector)
