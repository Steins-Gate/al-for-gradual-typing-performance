import torch
import numpy as np
from torch_geometric.data import Data, DataLoader
def customdataset(arr):
    X=[]
    for i in range(len(arr)):
        a=[0 for x in range(len(arr))]
        if arr[i]==1:
            a[i]=1
        X.append(a)
    return X
def generategraphdataset(X,y,graph):
    X_node=[]
    for a in X:
        x=customdataset(a)
        X_node.append(x)
    #print(len(X_node))
    X_node=np.array(X_node)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    train_pos_edge_index = torch.tensor(np.array(graph), dtype=torch.long).to(device)
    num_node, num_feature = int(np.amax(graph)+1), X.shape[1]
    #print(graph)
    #print(train_pos_edge_index)
    #print(num_node,num_feature)
    data_set=[]
    for i in range(len(y)):
        data = Data(x = torch.from_numpy(X_node[i]).float().to(device),
                    edge_index = train_pos_edge_index,
                    y = torch.from_numpy(y[i]).float().to(device))
        data_set.append(data)
    return data_set