import torch
from dataset import data_loader as loader
from dataset import pytorchgemetric_dataset as Dataset
from torch_geometric.data import DataLoader
from torch.nn import Linear
import torch.nn.functional as F
from torch_geometric.nn import GCNConv
from torch_geometric.nn import global_mean_pool
from torch_geometric.nn import GraphConv
from torch_geometric.nn import GINConv
import torch.nn as nn
from math import sqrt
import random
# Load the bit vectors
X, y = loader.synth()
print(len(X),len(X[0]),len(y))

# Load the call graph
from dataset import generate_synth as generator 
graph = generator.callgraph
print(graph)
# Generate graph dataset using bit vectors and call graph
dataset=Dataset.generategraphdataset(X,y,graph)
tempdata=dataset[50]
number_of_node_features=len(tempdata.x[0])
print(tempdata.y)
random.shuffle(dataset)

# Train test split
number_of_graph_in_training=100
train_dataset = dataset[:number_of_graph_in_training]
test_dataset = dataset[number_of_graph_in_training:]
train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True)
test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False)
print(len(train_loader.dataset))
for step, data in enumerate(train_loader):
    print(f'Step {step + 1}:')
    print('=======')
    print(f'Number of graphs in the current batch: {data.num_graphs}')
    print(data)
    print()

# Graph Isomorphism network
class GCN(torch.nn.Module):
    def __init__(self, hidden_channels):
        super(GCN, self).__init__()
        torch.manual_seed(12345)
        self.conv1 = GINConv( nn.Sequential(nn.Linear(number_of_node_features, hidden_channels),
                                  nn.ReLU(), nn.Linear(hidden_channels, hidden_channels)))
        self.conv2 = GINConv(nn.Sequential(nn.Linear(hidden_channels, hidden_channels),
                                  nn.ReLU(), nn.Linear(hidden_channels, hidden_channels)))
        self.conv3 = GINConv(nn.Sequential(nn.Linear(hidden_channels, hidden_channels),
                                  nn.ReLU(), nn.Linear(hidden_channels, hidden_channels)))
        self.lin = Linear(hidden_channels, 1)
        
        

    def forward(self, x, edge_index, batch):
        # 1. Obtain node embeddings 
        x = self.conv1(x, edge_index)
        x = x.relu()
        x = self.conv2(x, edge_index)
        x = x.relu()
        x = self.conv3(x, edge_index)
        
        # 2. Readout layer
        x = global_mean_pool(x, batch)  # [batch_size, hidden_channels]

        # 3. Apply a final classifier
        x = F.dropout(x, p=0.05, training=self.training)
        x = self.lin(x)
        
        return x.squeeze(1)

model = GCN(hidden_channels=64)
print(model)
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
criterion = torch.nn.L1Loss()

def train():
    model.train()

    for data in train_loader:  # Iterate in batches over the training dataset.
         out = model(data.x, data.edge_index, data.batch)  # Perform a single forward pass.
         loss = criterion(out, data.y)  # Compute the loss.
         #print(f'Loss is {loss.item()}')
         loss.backward()  # Derive gradients.
         optimizer.step()  # Update parameters based on gradients.
         optimizer.zero_grad()  # Clear gradients.
def test(loader):
     model.eval()

     correct = 0
     for data in loader:  # Iterate in batches over the training/test dataset.
         out = model(data.x, data.edge_index, data.batch)  
         pred = out  # Use the class with highest probability.
         #correct += int((pred == data.y).sum())  # Check against ground-truth labels
        
         correct += (abs((pred-data.y))).sum()
     return correct / len(loader.dataset)  # Derive ratio of correct predictions.
def avgerror(test_loader):
    model.eval()
    loss_func = torch.nn.L1Loss()
    loss_func2= torch.nn.MSELoss()
    
    total_loss=0
    total_mse=0
    for batch in test_loader:
        out = model(batch.x, batch.edge_index, batch.batch)
        pred=out
        label = batch.y
        loss = loss_func(pred, label)
        loss_mse=loss_func2(pred, label)
        total_loss += loss.item()
        total_mse+=loss_mse.item()
    total_loss/= len(test_loader.dataset)
    total_mse/=len(test_loader.dataset)
    total_mse=sqrt(total_mse)
    print(total_loss)
    print(total_mse)
def errorratio(test_loader):
    correct = 0
    result=0
    for batch in test_loader:
        out = model(batch.x, batch.edge_index, batch.batch)
        pred=out
        correct += (abs((pred-batch.y))/batch.y).sum()
    result=correct / len(test_loader.dataset)
    print(result.item())
for epoch in range(1, 201):
    train()
    train_acc = test(train_loader)
    test_acc = test(test_loader)
    print(f'Epoch: {epoch:03d}, Train Error: {train_acc:.4f}, Test Error: {test_acc:.4f}')
avgerror(test_loader)
errorratio(test_loader)