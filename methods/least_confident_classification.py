from sklearn.datasets import load_iris
from alipy.experiment.al_experiment import AlExperiment
from matplotlib import rcParams
from dataset import data_loader as loader

rcParams['font.family'] = 'Times New Roman'
rcParams['font.size'] = 20
rcParams["legend.columnspacing"] = 0.1

DATASET = 'Meteor'

if DATASET == 'Meteor':
    X, y = loader.meteor(clf=True)
elif DATASET == 'RayTrace':
    X, y = loader.raytrace(clf=True)
elif DATASET == "NBody":
    X, y = loader.nbody(clf=True)

# X, y = load_iris(return_X_y=True)
al = AlExperiment(X, y, stopping_criteria='num_of_queries', stopping_value=200)
al.split_AL()
al.set_query_strategy(strategy="QueryInstanceUncertainty", measure='least_confident')
al.set_performance_metric('accuracy_score')
al.start_query(multi_thread=True)
al.plot_learning_curve()